FROM node:14.3 AS build

WORKDIR /app
COPY package.json package.json
RUN npm install
COPY *.json /app/
COPY src src
RUN npm run build -- --prod --build-optimizer --configuration=production

FROM nginx:alpine

ENV ADHESION_API_URL http://app/v1
ENV ADHESION_URL http://app
ENV KEYCLOAK_CLIENT_ID ""
ENV KEYCLOAK_REALM "asso-insa-lyon"
ENV KEYCLOAK_URL "https://sso.asso-insa-lyon.fr/auth"
ENV MGMT_WS_URL "wss://mgmt-adhesion.asso-insa-lyon.fr"
ENV MGMT_URL "https://mgmt-adhesion.asso-insa-lyon.fr"
ENV MGMT_ROLE "mgmt"

COPY nginx/default.conf /etc/nginx/conf.d/default.conf
COPY nginx/start.sh /start.sh
COPY --from=build /app/dist/* /usr/share/nginx/html/
COPY --from=build /app/src/assets /usr/share/nginx/html/assets

CMD /start.sh
