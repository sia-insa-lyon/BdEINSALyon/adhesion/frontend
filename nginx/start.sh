#!/bin/sh -e

echo "Configuring UI to use APP Server: $ADHESION_URL"
sed -i -e \
  's#__ADHESION_API_URL__#'"$ADHESION_API_URL"'#g' \
  /usr/share/nginx/html/*.js
sed -i -e \
  's#__ADHESION_URL__#'"$ADHESION_URL"'#g' \
  /usr/share/nginx/html/*.js
sed -i -e \
  's#__KEYCLOAK_URL__#'"$KEYCLOAK_URL"'#g' \
  /usr/share/nginx/html/*.js
sed -i -e 's#__KEYCLOAK_CLIENT_ID__#'"$KEYCLOAK_CLIENT_ID"'#g' /usr/share/nginx/html/*.js
sed -i -e 's#__KEYCLOAK_REALM__#'"$KEYCLOAK_REALM"'#g' /usr/share/nginx/html/*.js
sed -i -e 's#__WEI_API_URL__#'"$WEI_API_URL"'#g' /usr/share/nginx/html/*.js
sed -i -e 's#__MOBILE_GATEWAY_URL__#'"$MOBILE_GATEWAY_URL"'#g' /usr/share/nginx/html/*.js
sed -i -e 's#__MGMT_WS_URL__#'"$MGMT_WS_URL"'#g' /usr/share/nginx/html/*.js
sed -i -e 's#__MGMT_URL__#'"$MGMT_URL"'#g' /usr/share/nginx/html/*.js
sed -i -e 's#__MGMT_ROLE__#'"$MGMT_ROLE"'#g' /usr/share/nginx/html/*.js

echo "removing old compressed files" #en fait y'a pas besoin
find /usr/share/nginx/html/ -name *.gz -exec sh -c 'rm $0' {} \;

echo "compressing files"
gzip -vk9 /usr/share/nginx/html/*

echo "Starting Web Server"
nginx -g 'daemon off;'
