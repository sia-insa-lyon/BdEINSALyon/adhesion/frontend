/*
import {Injectable} from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot, UrlTree,
} from '@angular/router';
import {KeycloakService} from 'keycloak-angular';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard {
  protected keycloakAngular: KeycloakService;
  protected authenticated: boolean;
  protected roles: string[];

  constructor(private router: Router, private keycloak: KeycloakService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return true;
  }

  public async isAccessAllowed(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    // Force the user to log in if currently unauthenticated.
    if (!this.authenticated) {
      await this.keycloak.login({
        redirectUri: window.location.origin + state.url,
      });
    }

    // Get the roles required from the route.
    const requiredRoles = route.data.roles;

    // Allow the user to to proceed if no additional roles are required to access the route.
    if (!(requiredRoles instanceof Array) || requiredRoles.length === 0) {
      return true;
    }

    // Allow the user to proceed if all the required roles are present.
    return requiredRoles.every((role) => this.roles.includes(role));
  }
}
*/
import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {KeycloakAuthGuard, KeycloakService} from 'keycloak-angular';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard extends KeycloakAuthGuard implements CanActivate {
  static maintenanceMode = false;

  constructor(protected router: Router, protected keycloakAngular: KeycloakService) {
    super(router, keycloakAngular);
  }

  isAccessAllowed(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
      /*if (this.authenticated) {
        return resolve(true);
      }*/
      if (!this.authenticated) {
        /*this.keycloakAngular.login()
          .catch(e => console.error(e));
        return reject(false);*/
        await this.keycloakAngular.login({
          redirectUri: window.location.origin + state.url,
        });
      }
      if (AuthGuard.maintenanceMode && this.roles.indexOf(environment.mgmt_role) < 0) {
        return reject(false);
      }

      const requiredRoles: string[] = route.data.roles;
      // const requiredRoles: string[] = ['staff'];
      console.log('roles ', this.roles);
      // Allow the user to to proceed if no additional roles are required to access the route.
      if (!(requiredRoles instanceof Array) || requiredRoles.length === 0) {
        return resolve(true);
      }

      // Allow the user to proceed if all the required roles are present.
      return resolve(requiredRoles.every((role) => this.roles.includes(role)));
    });
  }
}
