import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {KeycloakService} from 'keycloak-angular';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  @Output() isLogged = new EventEmitter<boolean>();
  loggedIn = false;

  constructor(private kc: KeycloakService) {
  }

  ngOnInit() {
    this.checkLogin();
  }

  checkLogin() {
    console.log('all riles', this.kc.getUserRoles());
    this.kc.isLoggedIn().then(v => {
      console.log('logged in:', v);
      this.loggedIn = v;
      if (!v) {
        setTimeout(this.checkLogin, 500);
      }
      if (v) {
        this.isLogged.emit(true);
      }
    });
  }

  login() {
    this.kc.login();
    console.log('login');
    this.kc.isLoggedIn().then((value) => {
      this.isLogged.emit(value);
      this.loggedIn = value;
    });
  }

  logout() {
    this.kc.logout();
    console.log('logout');
    this.isLogged.emit(false);
    this.loggedIn = false;
  }

  c() {
    this.kc.isLoggedIn().then(v => {
      console.log('cehck,', v);
      this.loggedIn = v;
    });
  }

}
