import {Component, OnInit} from '@angular/core';
import {MercanetService, TransactionMercanet} from '../../../data/mercanet.service';
import {FormControl, FormGroup} from '@angular/forms';
import {UiService} from '../../../data/ui.service';

@Component({
  selector: 'app-mercanet-search',
  templateUrl: './mercanet-search.component.html',
  styleUrls: ['./mercanet-search.component.css']
})
export class MercanetSearchComponent implements OnInit {
  transactions: TransactionMercanet[] = [];
  form: FormGroup;

  constructor(private mercanetService: MercanetService, private ui: UiService) {
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      search: new FormControl(''),
      responseCode: new FormControl(''),
      status: new FormControl(''),
      start_date: new FormControl(new Date()),
      start_time: new FormControl('00:00'),
      stop_date: new FormControl(new Date()),
      stop_time: new FormControl('23:59'),
    });
    this.ui.setTitle('MercaNET');
    this.ui.setLoading(true);
    this.mercanetService.list().subscribe((trs) => {
      this.transactions = trs;
      this.ui.setLoading(false);
    }, error => this.ui.setLoading(false));
  }

  filterAndSearch() {
    const filters = {};
    if (this.form.value.responseCode !== '') {
      filters['responseCode'] = this.form.value.responseCode;
    }
    if (this.form.value.search !== '') {
      filters['search'] = this.form.value.search;
    }
    if (this.form.value.status !== '') {// && this.form.value.status !== undefined) {
      filters['status'] = this.form.value.status;
    }

    const start_datetime: Date = new Date(this.form.value.start_date);
    const stop_datetime: Date = new Date(this.form.value.stop_date);

    const start_time = this.form.value.start_time.split(':');
    start_datetime.setHours(start_time[0]);
    start_datetime.setMinutes(start_time[1]);

    const stop_time = this.form.value.stop_time.split(':');
    stop_datetime.setHours(stop_time[0]);
    stop_datetime.setMinutes(stop_time[1]);
    try {
      if (this.form.value.start_date !== '') {
        filters['start_datetime'] = start_datetime.toISOString();
        filters['stop_datetime'] = stop_datetime.toISOString();
      }
    } catch (e) {
    }

    console.log(this.form.value, filters);
    this.ui.setLoading(true);
    this.mercanetService.filter(filters).subscribe((trs) => {
      this.transactions = trs;
      this.ui.setLoading(false);
    }, error => {
      this.transactions = [];
      this.ui.setLoading(false);
    });
  }

}
