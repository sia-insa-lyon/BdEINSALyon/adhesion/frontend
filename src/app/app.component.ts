import {Component, OnInit} from '@angular/core';
import {UiService} from './data/ui.service';
import {Subscription} from 'rxjs';
import {QrViewComponent} from './companion/qr-view/qr-view.component';
import {CompanionService} from './data/companion.service';
import {CardService} from './data/members.service';
import {NavigationEnd, Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {ChatMessageEvent, ControlPanelService, LoggerService, ROUTE_CHANGE} from './data/management.service';
import {KeycloakService} from 'keycloak-angular';
import {ChatMessagePopupComponent} from './mgmt/chat-message-popup/chat-message-popup.component';
import {AuthGuard} from './users/auth-guard.service';
import {environment} from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  public title = 'Accueil';
  titleSubsciption: Subscription;
  loadingSubscription: Subscription;
  public isLoading = false;
  public isLogged = false;
  public wsStatus = 'unconnected'; // connected then disconnected
  navigateToMember = true;
  wsTimeout = null;

  heartbeat = false;
  username = 'anon';
  historique: String[] = [];

  constructor(private ui: UiService,
              public dialog: MatDialog,
              private sync: CompanionService,
              private cardapi: CardService,
              private router: Router,
              private snackBar: MatSnackBar,
              private logger: LoggerService,
              private kcs: KeycloakService,
              private cps: ControlPanelService) {
    this.titleSubsciption = this.ui.getTitle().subscribe(titre => {
      this.title = titre;
    });
    this.loadingSubscription = this.ui.getLoadingState().subscribe(x => {
      this.isLoading = x;
    });
    this.isLogged = false;
    this.wsStatus = 'unconnected';
    this.router.routeReuseStrategy.shouldReuseRoute = function (future, curr) {
      // sinon au 2e scan on n'est pas redirigé
      return false;
    };
  }

  ngOnInit() {
    console.log('hello');
    this.cardapi.getUsername().subscribe(u => {
      this.username = u.username;
    });
    this.kcs.getToken().then((jwt) => {
      console.log(jwt);
      this.logger.init(jwt);
      this.listenMessages();
      this.logRoute();
    });
    this.cps.getWebConfig().subscribe(wbcfg => {
      if (!wbcfg.admin_frontend_enabled) {
        this.setModeMaintenance();
      } else {
        AuthGuard.maintenanceMode = false;
      }
    });
  }

  listenMessages() {
    this.logger.subscribe((msg: ChatMessageEvent) => {
      if (msg.type === 'dialog') {
        this.dialog.open(ChatMessagePopupComponent, {
          data: msg.data,
          disableClose: true,
        });
      }
      if (msg.type === 'snackbar') {
        this.snackBar.open(msg.data, 'Fermer');
      }
      if (msg.type === 'maintenance') {
        if (msg.data) {
          this.setModeMaintenance();
        } else {
          this.router.navigateByUrl('/');
          this.title = 'Adhésion';
          AuthGuard.maintenanceMode = false;
        }
      }
      console.log('logger:websocket message ', msg);
    });
  }

  logRoute() {
    this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationEnd) {
        // Hide loading indicator
        this.logger.log(ROUTE_CHANGE, ev.url);
        console.log(ev);
      }
    });
  }

  setModeMaintenance() {
    this.title = 'Mode maintenance';
    if (this.kcs.getUserRoles(true).indexOf(environment.mgmt_role) < 0) { // on ne force pas pour les managers
      AuthGuard.maintenanceMode = true;
      this.router.navigateByUrl('maintenance');
    }
  }

  showQrCode() {
    this.sync.chaussetteInternet.next('hey');
    if (this.wsStatus !== 'connected' && this.isLogged) {
      const dialogRef = this.dialog.open(QrViewComponent, {
        // data: sessionStorage.getItem('access_token'),
        data: this.sync.wsIdentifierToken,
      });
      this.sync.observableSync.subscribe(data => {
          this.historique.push(JSON.stringify(data));
          console.log('ws:', data);
          if (/c([0-9]{12})/.test(data['code']) && data['type'] === 'code_va') {
            this.historique.push('Code VA, essai de récup');
            this.cardapi.getMember(data['code']).subscribe(
              m => {
                this.historique.push(m.first_name + ' ' + m.last_name);
                this.snackBar.open('Code VA: ' + data['code'], 'Fermer');
                if (this.navigateToMember) {
                  this.router.navigateByUrl('/view/' + m.id);
                }
              },
              er => {
                this.historique.push('erreur ' + er.toString());
              }
            );
          }
          if (data['type'] === 'status_message' && data['status'] === 'connected') {
            this.beat();
            dialogRef.close();
            if (this.wsStatus === 'unconnected') {
              this.sync.chaussetteInternet.next({type: 'status_message', status: 'HELO'});
            } else {
              this.sync.chaussetteInternet.next({type: 'status_message', status: 'ACK'});
            }
            this.wsStatus = 'connected';
            this.snackBar.open('Connecté au téléphone !', 'Ok', {
              duration: 2500,
            });
            // this.phoneConnected = true;
          }
          if (data['type'] === 'status_message' && data['status'] === 'disconnected') {
            this.handlerMobileTimeout();
          }
          if (data['type'] === 'status_message' && data['status'] === 'keepalive') {
            this.beat();
            if (this.wsTimeout !== null) {
              clearTimeout(this.wsTimeout);
            }
            this.wsTimeout = setTimeout(() => {
              this.handlerMobileTimeout();
            }, 12000);
          }
        },
        error => {
          // this.error = error;
        },
        () => {
          this.wsStatus = 'disconnected';
          //  this.completed = true;
        });
    } else {
      // this.trigger.openMenu();
    }
    //  this.trigger.menu = this.syncMenu;
  }

  fermerWs() {
    this.sync.chaussetteInternet.complete();
  }

  handlerMobileTimeout() {
    this.fermerWs();
    this.wsStatus = 'disconnected';
    this.snackBar.open('Perte de la connexion au téléphone !');
  }

  beat() {
    this.heartbeat = !this.heartbeat;
    setTimeout(() => {
      this.heartbeat = !this.heartbeat;
    }, 1000);
  }

  enableButtons() {
    return this.isLogged && !AuthGuard.maintenanceMode;
  }
}
