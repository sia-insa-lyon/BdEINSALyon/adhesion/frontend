import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {ListComponent} from './members/list/list.component';
import {EditComponent} from './members/edit/edit.component';
import {GenerateCodeComponent} from './generate-code/generate-code.component';
import {AuthGuard} from './users/auth-guard.service';
import {MemberViewComponent} from './members/view/view.component';
import {InteComponent} from './inte/inte.component';
import {ResetComponent} from './inte/reset/reset.component';
import {PreenregistrementComponent} from './inte/preenregistrement/preenregistrement.component';
import {CompanionComponent} from './companion/companion.component';
import {WeiComponent} from './wei/wei/wei.component';
import {TableauComponent} from './wei/tableau/tableau.component';
import {BizuthEditComponent} from './wei/bizuth-edit/bizuth-edit.component';
import {BusComponent} from './wei/bus/bus.component';
import {BusDetailComponent} from './wei/bus/detail/bus-detail.component';
import {BungalowListComponent} from './wei/bungalow/bungalow-list/bungalow-list.component';
import {BungalowDetailComponent} from './wei/bungalow/bungalow-detail/bungalow-detail.component';
import {MembershipFeuilleCaisseComponent} from './members/membership/membership-feuille-caisse/membership-feuille-caisse.component';
import {MercanetSearchComponent} from './compta/mercanet/mercanet-search/mercanet-search.component';
import {WatchTowerComponent} from './mgmt/watchtower/watch-tower.component';
import {ControlPanelComponent} from './mgmt/control-panel/control-panel.component';
import {MaintenanceComponent} from './mgmt/maintenance/maintenance.component';
import {environment} from '../environments/environment';

const routes: Routes = [
  {path: 'maintenance', component: MaintenanceComponent},
  {
    path: '', canActivate: [AuthGuard], data: {roles: ['staff']},
    children: [
      {path: '', component: HomeComponent},
      {path: 'companion', component: CompanionComponent},
      {path: 'inte', component: InteComponent, canActivate: [AuthGuard]},
      {path: 'preenregistrer', component: PreenregistrementComponent, canActivate: [AuthGuard]},
      {path: 'reset/:url', component: ResetComponent},
      {path: 'code', component: GenerateCodeComponent},
      {path: 'members', component: ListComponent, canActivate: [AuthGuard]},
      {path: 'members/create', component: EditComponent, canActivate: [AuthGuard]},
      {path: 'members/:id', component: EditComponent, canActivate: [AuthGuard]},
      {path: 'view/:id', component: MemberViewComponent, canActivate: [AuthGuard]},
      {path: 'mercanet', component: MercanetSearchComponent, canActivate: [AuthGuard]},
      // {path: 'watch-tower', component: WatchTowerComponent, canActivate: [AuthGuard]},
      {path: 'feuille-caisse', component: MembershipFeuilleCaisseComponent},
      {
        path: 'wei', component: WeiComponent, canActivate: [AuthGuard],
        children: [
          {path: 'tableau', component: TableauComponent},
          {path: 'bizuth/:id', component: BizuthEditComponent},
          {path: 'bus', component: BusComponent},
          {path: 'bus/:id', component: BusDetailComponent},
          {path: 'bungalow', component: BungalowListComponent},
          {path: 'bungalow/:id', component: BungalowDetailComponent},
        ]
      },

    ]
  },
  {
    path: 'mgmt', data: {roles: [environment.mgmt_role]}, canActivate: [AuthGuard],
    children: [
      {path: 'controlpanel', component: ControlPanelComponent},
      {path: 'watchtower', component: WatchTowerComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
