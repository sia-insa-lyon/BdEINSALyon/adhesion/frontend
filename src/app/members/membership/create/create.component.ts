import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CardService, Membership, MembershipService, MembershipType} from '../../../data/members.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-membership-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class MemberShipCreateComponent implements OnInit {
  memberShipTypes: MembershipType[];
  @Input() idAdherent: number;
  @Output() out = new EventEmitter<Membership>();
  @Output() typeOut = new EventEmitter<MembershipType>();
  form: FormGroup;
  membership: Membership;
  boutonInactif: boolean;
  saved: boolean;
  errors;

  constructor(private service: MembershipService, private fb: FormBuilder, public snackBar: MatSnackBar, private cardService: CardService) {
    this.form = this.fb.group({
      membership: '',
      valid: true,
      payment_method: 3,
      //card_id: ''
    });
  }

  ngOnInit() {
    this.boutonInactif = false;
    console.log(this.idAdherent);
    if (this.idAdherent) {
      this.service.filterTypes(this.idAdherent).subscribe(x => {
        this.memberShipTypes = x;
        let cheapestM: MembershipType = this.memberShipTypes[0];
        for (const type of this.memberShipTypes) {
          if (type.price_ttc < cheapestM.price_ttc) {
            cheapestM = type;
          }
        }
        this.form.patchValue({membership: cheapestM, payment_method: '3', valid: true});
      });
    }
  }

  save(formData) {
    if (this.form.valid) {
      this.boutonInactif = true;
      const data = new Membership();
      data.membership = formData.membership.name;
      data.valid = formData.valid;
      data.member = this.idAdherent;
      data.payment_method = formData.payment_method;
      this.service.create(data).subscribe(x => {
        this.membership = x;
        this.saved = true;
        this.out.emit(x);
        this.typeOut.emit(formData.membership);
        this.snackBar.open('Adhésion enregistrée', 'OK', {duration: 2000});
        this.boutonInactif = false;
        //this.createCard(formData);
      }, error => {
        this.boutonInactif = false;
        this.errors = error;
      });
      console.log(formData);
      console.log(data);
    }
  }

  /*createCard(formData) {
    if (/c([0-9]{12})/.test(formData.card_id)) {
      const card = new Card();
      card.code = formData.card_id;
      card.member = this.idAdherent;
      card.activated = true;
      this.cardService.create(card).subscribe(
        next => {
          this.snackBar.open('Carte VA créée', 'OK', {duration: 2000});
          this.boutonInactif = false;
        },
        error => this.errors = error
      );
    }
  }*/

  condToStr(cond: string) {
    if (cond === 'YEAR_IS') {
      return 'égal';
    }
    if (cond === 'YEAR_GT') {
      return 'supérieur';
    }
    if (cond === 'YEAR_LT') {
      return 'inférieur';
    }
  }

}
