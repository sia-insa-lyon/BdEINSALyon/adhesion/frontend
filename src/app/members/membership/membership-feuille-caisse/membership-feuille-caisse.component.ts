import {Component, OnInit} from '@angular/core';
import {FeuilleCaisseMemberships, MembershipService} from '../../../data/members.service';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-membership-feuille-caisse',
  templateUrl: './membership-feuille-caisse.component.html',
  styleUrls: ['./membership-feuille-caisse.component.scss']
})
export class MembershipFeuilleCaisseComponent implements OnInit {
  data: FeuilleCaisseMemberships = new FeuilleCaisseMemberships();
  form: FormGroup;

  hideDisabledTypes = false;

  constructor(private api: MembershipService) {
  }


  ngOnInit() {
    this.form = new FormGroup({
      start_date: new FormControl(new Date()),
      start_time: new FormControl('00:00'),
      stop_date: new FormControl(new Date()),
      stop_time: new FormControl('23:59'),
    });
    this.api.feuilleCaisseToday().subscribe(
      d => {
        this.data = d;
      }
    );
  }

  fetch(formData) {
    //console.log('form data:', formData);
    const start_datetime: Date = new Date(formData.start_date);
    const stop_datetime: Date = new Date(formData.stop_date);

    const start_time = formData.start_time.split(':');
    start_datetime.setHours(start_time[0]);
    start_datetime.setMinutes(start_time[1]);

    const stop_time = formData.stop_time.split(':');
    stop_datetime.setHours(stop_time[0]);
    stop_datetime.setMinutes(stop_time[1]);

    //console.log('datetime start', start_datetime, ' stop ', stop_datetime);
    this.api.feuilleCaisse(start_datetime, stop_datetime).subscribe(
      d => {
        this.data = d;
      }
    );
  }

}
