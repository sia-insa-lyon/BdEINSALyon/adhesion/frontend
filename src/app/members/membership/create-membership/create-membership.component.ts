import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Membership, MembershipService, MembershipType} from '../../../data/members.service';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-create-membership',
  templateUrl: './create-membership.component.html',
  styleUrls: ['./create-membership.component.scss']
})
export class CreateMembershipComponent implements OnInit {
  @Input() idAdherent: number;
  @Output() out = new EventEmitter<Membership>();
  form: FormGroup;
  memberShipTypes: MembershipType[] = [];
  boutonInactif = false;
  errors: string;


  constructor(private service: MembershipService, private fb: FormBuilder) {
    this.form = this.fb.group({
      membership: '',
      valid: true,
      payment_method: 3,
    });
  }

  ngOnInit() {
    if (this.idAdherent !== null) {
      this.service.filterTypes(this.idAdherent).subscribe(types => {
        this.memberShipTypes = types;
        this.form.patchValue({membership: '' + this.memberShipTypes[0].name, payment_method: '3', valid: true});
      });
    }
  }

  formSubmit(formData) {
    this.boutonInactif = true;
    const data = new Membership();
    data.membership = formData.membership;
    data.valid = formData.valid;
    data.member = this.idAdherent;
    data.payment_method = formData.payment_method;
    this.service.create(data).subscribe(membership => {
      this.out.emit(membership);
      this.boutonInactif = false;
    }, error => {
      this.errors = error;
    });
  }

  condToStr(cond: string) {
    if (cond === 'YEAR_IS') {
      return 'égal';
    }
    if (cond === 'YEAR_GT') {
      return 'supérieur';
    }
    if (cond === 'YEAR_LT') {
      return 'inférieur';
    }
  }
}
