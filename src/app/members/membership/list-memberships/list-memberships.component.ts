import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Card, Member, MembershipService, MembershipType, method_to_str} from '../../../data/members.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-list-memberships',
  templateUrl: './list-memberships.component.html',
  styleUrls: ['./list-memberships.component.scss']
})
export class ListMembershipsComponent implements OnInit {
  @Input() member!: Member;
  membership_types: Map<string, MembershipType> = new Map<string, MembershipType>();
  @Output() out = new EventEmitter<Member>();

  constructor(private sb: MatSnackBar, private membershipService: MembershipService) { }

  ngOnInit(): void {
    const id = this.member.id;
    console.log('Id de l\'utilisateur Adhésion: ' + id);
    this.membershipService.listTypes().subscribe((ms) => {
      ms.forEach((m) => {
        this.membership_types[m.name] = m;
      });
    });
  }

  payment_method_to_str(pm) {
    return method_to_str(pm);
  }

  deleteMembership(membership) {
    this.membershipService.delete(membership.id).subscribe(x => {
      this.out.emit(this.member);
      this.sb.open('Adhésion effacée !', 'Annuler', {duration: 3000}).onAction().subscribe(() => {
        this.membershipService.create(membership).subscribe(() => {
          this.out.emit(this.member);
          this.sb.open('Adhésion restaurée !', '', {duration: 2500});
        });
      });
    });
  }

  enableMembership(membership) {
    membership.valid = true;
    this.membershipService.update(membership).subscribe(x => {
      this.out.emit(this.member);
      this.sb.open('Adhésion activée !', '', {duration: 2500});
    });
  }
  disableMembership(membership) {
    membership.valid = false;
    this.membershipService.update(membership).subscribe(x => {
      this.out.emit(this.member);
      this.sb.open('Adhésion désactivée !', '', {duration: 2500});
    });
  }

}
