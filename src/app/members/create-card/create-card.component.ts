import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Card, CardService} from '../../data/members.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-create-card',
  templateUrl: './create-card.component.html',
  styleUrls: ['./create-card.component.scss']
})
export class CreateCardComponent implements OnInit {
  @Input() idAdherent: number;
  @Output() card = new EventEmitter<Card>();
  form: FormGroup;
  errors;
  boutonInactif = false;

  constructor(private service: CardService, private fb: FormBuilder, private snackBar: MatSnackBar) {
    this.form = this.fb.group({
      card_id: ''
    });
  }

  ngOnInit() {
    console.log(this.idAdherent);
  }

  save(formData) {
    if (/c([0-9]{12})/.test(formData.card_id)) {
      this.boutonInactif = true;
      const card = new Card();
      card.code = formData.card_id;
      card.member = this.idAdherent;
      card.activated = true;
      this.service.create(card).subscribe(
        next => {
          this.snackBar.open('Carte VA associée', 'OK', {duration: 2000});
          this.card.emit(card);
          this.boutonInactif = false;
          this.resetForm();
        },
        error => {
          if (error.error.detail === 'Cette carte VA est déjà attribuée !'
            || error.error.detail === 'Il n\'y a pas de carte VA avec ce code !') {
            this.snackBar.open(error.error.detail, 'OK', {duration: 2000});
          } else {
            this.errors = error;
          }
          this.boutonInactif = false;
        }
      )
      ;
    } else {
      this.snackBar.open('Le code de la carte doit être de la forme cX (X étant un nombre à 12 chiffres)', 'OK', {duration: 2000});
    }
  }

  resetForm() {
    this.form.reset();

    Object.keys(this.form.controls).forEach(key => {
      const control = this.form.get(key);
      control?.markAsPristine();
      control?.markAsUntouched();
      control?.updateValueAndValidity();
    });
  }

}
