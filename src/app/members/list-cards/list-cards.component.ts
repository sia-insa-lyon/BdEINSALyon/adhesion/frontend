import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Card, CardService, Member,} from '../../data/members.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-list-cards',
  templateUrl: './list-cards.component.html',
  styleUrls: ['./list-cards.component.scss']
})
export class ListCardsComponent implements OnInit {
  @Input() member!: Member;
  @Output() out = new EventEmitter<Card>();
  errors;

  constructor(private service: CardService, public snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
  }

  dissociateCard(card) {
    card.member = null;
    this.service.update(card).subscribe(value => {
      this.out.emit(card);
      this.snackBar.open('Carte dissociée', 'Annuler', {duration: 3000}).onAction().subscribe(() => {
        card.member = this.member.id;
        this.service.update(card).subscribe(() => {
          this.out.emit(card);
          this.snackBar.open('Carte restaurée', '', {duration: 2500});
        });
      });
    }, error => this.errors = error);
  }

  disableCard(card) {
    card.activated = false;
    this.service.update(card).subscribe(value => {
      this.snackBar.open('Carte désactivée', 'OK', {duration: 2000});
    }, error => this.errors = error);
  }

  enableCard(card) {
    card.activated = true;
    this.service.update(card).subscribe(value => {
      this.snackBar.open('Carte activée', 'OK', {duration: 2000});
    }, error => this.errors = error);
  }
}
