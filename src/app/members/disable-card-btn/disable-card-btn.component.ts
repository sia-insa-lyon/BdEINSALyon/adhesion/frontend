import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Card, CardService} from '../../data/members.service';

@Component({
  selector: 'app-disable-card-btn',
  templateUrl: './disable-card-btn.component.html',
  styleUrls: ['./disable-card-btn.component.scss']
})
export class DisableCardBtnComponent implements OnInit {
  @Input() card: Card = null;
  @Output() updatedCard = new EventEmitter<Card>();

  constructor(private cardService: CardService) {
  }

  ngOnInit() {
  }

  deactivateCard() {
    if (this.card !== null) {
      const card = {...this.card};
      card.activated = false;
      this.cardService.update(card).subscribe(c => this.updatedCard.emit(c));
    }
  }
}
