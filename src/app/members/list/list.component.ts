import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Component, OnInit} from '@angular/core';
import {MembersService, SearchResult} from '../../data/members.service';

import {Subject, Subscription} from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  lastSearchResult: SearchResult;
  term$ = new Subject<string>();
  loading = false;
  searchbox: string;
  onlyBiz = false;
  private _searchSubscription: Subscription = null;

  constructor(private members: MembersService) {
  }

  ngOnInit() {
    this.term$.pipe(debounceTime(300), distinctUntilChanged()).subscribe((term) => {
      console.log('term:', term);
      if (term === '') {
        this.lastSearchResult = null;
        return;
      }
      this.performSearch(term);
    });
  }

  performSearch(term) {
    if (this._searchSubscription) {
      this._searchSubscription.unsubscribe();
    }
    this.loading = true;
    this._searchSubscription = this.members.search(term, 1, {biz: this.onlyBiz}).subscribe((result) => {
      this._searchSubscription = null;
      this.lastSearchResult = result;
      this.loading = false;
    });
  }

  cbChange() {
    this.performSearch(this.searchbox);
  }

}
