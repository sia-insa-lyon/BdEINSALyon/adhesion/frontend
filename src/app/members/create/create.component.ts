import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MetadataService, StudyDepartment, StudySchool, StudyYear} from '../../data/metadata.service';
import {Member, MembersService, StudentProfile} from '../../data/members.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {UiService} from '../../data/ui.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss', '../edit/edit.component.scss']
})
export class CreateComponent implements OnInit {

  member: Member = new Member();
  form: FormGroup;
  departments: StudyDepartment[];
  schools: StudySchool[];
  years: StudyYear[];
  loading = false;
  errors;
  @Input() isEmbedded: boolean;
  @Output() out = new EventEmitter<Member>();

  constructor(private meta: MetadataService, private members: MembersService, private router: Router, private ui: UiService) {
  }

  ngOnInit() {
    this.meta.getDepartments().subscribe((deps) => {
      this.departments = deps;
    }, error => this.errors = error);
    this.meta.getSchools().subscribe((schools) => {
      this.schools = schools;
    }, error => this.errors = error);
    this.meta.getYears().subscribe((years) => {
      this.years = years;
    }, error => this.errors = error);
    this.form = new FormGroup({
      id: new FormControl(''),
      first_name: new FormControl('', Validators.required),
      last_name: new FormControl(''),
      email: new FormControl(''),
      phone: new FormControl('',),
      gender: new FormControl(''),
      birthday: new FormControl(''),
      has_valid_membership: new FormControl(false),
      student_profile: new FormGroup({
        id: new FormControl(),
        study_year: new FormControl(),
        department: new FormControl(),
        school: new FormControl(),
        student_number: new FormControl(),
      }),
      category: new FormControl(''),
    });
    // marche pas :(
    /*this.form.valueChanges.debounceTime(500).distinctUntilChanged((a, b) => {
      let x = a.first_name !== b.first_name && a.label !== b.last_name;
      console.log(x);
      return x;
    }).subscribe(w =>
      this.form.patchValue({email: this.form.value.first_name.toLowerCase() + '.'
        + this.form.value.last_name.toLowerCase() + '@insa-lyon.fr'})
    );*/
  }

  save(data) {
    if (this.form.valid) {
      this.loading = true;
      this.ui.setLoading(true);
      if (data.category !== 'student') {
        data.student_profile = null;
      } else {
        if (data.student_profile.study_year === '1A') {
          if (data.student_profile.student_number === '') {
            data.student_profile.student_number = null;
          }
        }
      }
      this.members.create(data).subscribe((member) => {
        member.student_profile = new StudentProfile();
        this.member = member;
        this.form.patchValue(member);
        this.loading = false;
        if (this.isEmbedded) {
          this.out.emit(member);
        } else {
          this.router.navigateByUrl('/view/' + this.member.id);
        }
      }, (response) => {

        const error = response.error;
        for (const field in error) {
          if (error.hasOwnProperty(field)) {
            this.form.get(field).setErrors({
              'incorrect': true
            });
          }
        }
        response => this.errors = response;
        this.loading = false;
      });
    } else {
      error => this.errors = error;
      console.log('Member form has errors');
      console.log(this.form.errors);
    }
  }

  autoFillEmail() {
    this.form.patchValue({
      email: this.form.value.first_name.toLowerCase() + '.'
        + this.form.value.last_name.toLowerCase() + '@insa-lyon.fr'
    });
  }
}

