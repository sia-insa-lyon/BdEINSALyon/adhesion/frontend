import {Component, Input, OnInit} from '@angular/core';
import {Member} from '../../../data/members.service';

@Component({
  selector: 'app-va-cheque-received-info-span',
  templateUrl: './va-cheque-received-info-span.component.html',
  styleUrls: ['./va-cheque-received-info-span.component.scss']
})
export class VaChequeReceivedInfoSpanComponent implements OnInit {
  @Input() member: Member;

  constructor() {
  }

  ngOnInit() {
  }
}
