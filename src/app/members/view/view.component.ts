import {Component, Input, OnInit} from '@angular/core';
import {CardService, Member, MembershipService, MembershipType, MembersService, method_to_str} from '../../data/members.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class MemberViewComponent implements OnInit {
  member: Member;
  errors: string;
  @Input() membreid: number;
  membership_types: Map<string, MembershipType> = new Map<string, MembershipType>();

  constructor(private members: MembersService, private activeRoute: ActivatedRoute, private cardService: CardService,
              private sb: MatSnackBar, private router: Router, private membershipService: MembershipService) {
  }

  ngOnInit() {

    const id = this.membreid ? this.membreid : this.activeRoute.snapshot.params['id'];
    console.log('Id de l\'utilisateur Adhésion: ' + id);
    this.refresh(id);
    this.membershipService.listTypes().subscribe((ms) => {
      ms.forEach((m) => {
        this.membership_types[m.name] = m;
      });
    });

  }

  refresh(id: number) {
    this.members.retrieve(id).subscribe(
      member => this.member = member,
      error => this.errors = 'Une erreur est arrivée lors de la récupération d\'un adhérent'
    );
  }

  showCardDeactivatedInfo() {
    this.sb.open('La carte a été désactivée', 'ok');
    this.refresh(this.member.id);
  }

  delete() {
    this.sb.open('Voulez-vous vraiment supprimer cet adhérent ?', 'oui', {duration: 2500}).onAction().subscribe(() => {
      this.members.delete(this.member.id).subscribe(x => {
        this.router.navigateByUrl('/members');
        this.sb.open('Adhérent effacé !', '', {duration: 2500});
      });
    });
  }
}
