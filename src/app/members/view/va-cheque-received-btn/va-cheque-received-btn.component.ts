import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Member, MembersService} from '../../../data/members.service';

@Component({
  selector: 'app-va-cheque-received-btn',
  templateUrl: './va-cheque-received-btn.component.html',
  styleUrls: ['./va-cheque-received-btn.component.scss']
})
export class VaChequeReceivedBtnComponent implements OnInit {
  @Input() member: Member;
  @Output() updatedMember = new EventEmitter<Member>();

  constructor(private members: MembersService) {
  }

  ngOnInit() {
  }

  updateVAChequeReceived(ok: boolean) {
    this.members.patch_VA_cheque_received(this.member, ok).subscribe(m => this.updatedMember.emit(m));
  }
}
