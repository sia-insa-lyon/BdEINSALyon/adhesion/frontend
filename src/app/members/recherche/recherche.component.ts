import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Member, MembersService, SearchResult} from '../../data/members.service';

import {Subject, Subscription} from 'rxjs';

/**
 * Ce composant (réutilisable) permet d'obtenir un membre adhésion depuis une barre de recherche
 */
@Component({
  selector: 'app-recherche',
  templateUrl: './recherche.component.html',
  styleUrls: ['./recherche.component.scss']
})
export class RechercheAdherentComponent implements OnInit {

  lastSearchResult: SearchResult;
  term$ = new Subject<string>();
  public selectedMember: Member = null;
  @Output() out = new EventEmitter<Member>();
  errors;
  loading = false;
  searchbox: void;
  private _searchSubscription: Subscription = null;

  constructor(private members: MembersService) {
  }

  /**
   * Choisit un membre.
   * Marche aussi bien en mode ViewChild que template-binding
   * @param {Member} member
   * @returns {Member}
   */
  select(member: Member) {
    this.lastSearchResult = null;
    this.selectedMember = member;
    this.out.emit(member);
    return member;
  }

  ngOnInit() {
    this.term$.pipe(debounceTime(300), distinctUntilChanged()).subscribe((term) => {
      if (term === '') {
        this.lastSearchResult = null;
        return;
      }
      if (this._searchSubscription) {
        this._searchSubscription.unsubscribe();
      }
      this.loading = true;
      this._searchSubscription = this.members.search(term).subscribe((result) => {
        this._searchSubscription = null;
        this.lastSearchResult = result;
        this.loading = false;
      });
    }, error => this.errors = error);
  }

}
