import {Component, OnInit} from '@angular/core';
import { forkJoin } from 'rxjs';
import {InfosWei, WeiService} from '../../data/wei';

@Component({
  selector: 'app-wei',
  templateUrl: './wei.component.html',
  styleUrls: ['./wei.component.scss']
})
export class WeiComponent implements OnInit {
  total_bizuths = 0;
  total_places = 0;
  remplissage_bus = 0;
  remplissage_bungalow = 0;
  participants = 0;
  dateWei: string;

  constructor(private wei: WeiService) {
  }

  ngOnInit() {
    forkJoin([
      this.wei.getStats(),
      this.wei.getInfoWei()
    ]).subscribe(([statsData, InfoData]) => {
      const formatedDate = new Date(InfoData.date_wei).toLocaleDateString('fr-FR',{
        weekday: 'long',
        month: 'long',
        day: 'numeric',
      });
      this.dateWei = formatedDate;
      this.total_bizuths = statsData['total_bizuths'];
      this.total_places = statsData['total_places'];
      this.remplissage_bus = statsData['remplissage_bus'];
      this.remplissage_bungalow = statsData['remplissage_bungalow'];
      this.participants = statsData['participants'];
    });
  }

}
