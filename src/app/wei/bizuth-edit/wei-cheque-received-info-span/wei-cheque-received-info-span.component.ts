import {Component, Input, OnInit} from '@angular/core';
import {Bizuth} from '../../../data/wei';

@Component({
  selector: 'app-wei-cheque-received-info-span',
  templateUrl: './wei-cheque-received-info-span.component.html',
  styleUrls: ['./wei-cheque-received-info-span.component.scss']
})
export class WeiChequeReceivedInfoSpanComponent implements OnInit {
  @Input() bizuth: Bizuth;

  constructor() {
  }

  ngOnInit() {
  }
}
