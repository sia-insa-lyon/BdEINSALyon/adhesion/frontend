import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Bizuth, WeiService} from '../../../data/wei';
import {UiService} from '../../../data/ui.service';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-wei-moyen-paiement',
  templateUrl: './wei-moyen-paiement.component.html',
  styleUrls: ['./wei-moyen-paiement.component.scss']
})
export class WeiMoyenPaiementComponent implements OnInit {
  @Input() bizuth: Bizuth;
  @Output() out = new EventEmitter<Bizuth>();
  form: FormGroup;

  @Input() showButton = true;
  @Output() changed = new EventEmitter<number>();

  constructor(private wei: WeiService, private ui: UiService) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      payment_method: new FormControl('3'),
    });
    setTimeout(() => this.form.patchValue({'payment_method': '' + this.bizuth.payment_method}), 500);
  }

  update(formData) {
    console.log(formData);
    if (formData !== undefined) {
      this.wei.patchMoyenPaiement(this.bizuth, this.form.value.payment_method).subscribe(b => this.out.emit(b));
    }
  }

  change(ev) {
    //console.log('change', ev);
    this.changed.emit(parseInt(ev.value, 10));
  }

}
