import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Bizuth, WeiService} from '../../../data/wei';

@Component({
  selector: 'app-wei-delete-bizuth',
  templateUrl: './wei-delete-bizuth.component.html',
  styleUrls: ['./wei-delete-bizuth.component.scss']
})
export class WeiDeleteBizuthComponent implements OnInit {
  @Input() bizuth: Bizuth;
  @Output() out = new EventEmitter<Bizuth>();

  constructor(private wei: WeiService) {
  }

  ngOnInit() {
  }

  deleteBizuth() {
    return this.wei.deleteBizuth(this.bizuth).subscribe(
      b => this.out.emit(b)
    );
  }

}
