import {Component, OnInit} from '@angular/core';
import {Bizuth, dossierValide, WeiService} from '../../data/wei';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup} from '@angular/forms';
import {Member, MembershipService, MembersService} from '../../data/members.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-bizuth-edit',
  templateUrl: './bizuth-edit.component.html',
  styleUrls: ['./bizuth-edit.component.scss']
})
export class BizuthEditComponent implements OnInit {
  bizuth: Bizuth = null;
  form: FormGroup;

  constructor(private wei: WeiService, private members: MembersService, private activeRoute: ActivatedRoute, private membershipApi: MembershipService, private router: Router, private sb: MatSnackBar) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      bus: new FormControl(),
      bungalow: new FormControl(),
      regime_particulier: new FormControl(''),
      informations_speciales: new FormControl(''),
    });
    this.wei.getBizuth(this.activeRoute.snapshot.params['id']).subscribe(b => {
      if (b !== null) {
        this.setBizuth(b);
      }
    });
  }

  setBizuth(biz: Bizuth) {
    console.log('set bizuth:', biz);
    if (biz === null) {
      console.log('erreur de logique; setBizuth(null)');
      return;
    }
    this.bizuth = biz;
    if (this.bizuth.bus !== null) {
      if (isNaN(this.bizuth.bus)) {
        this.bizuth.bus = this.bizuth.bus.id;
      }
    }
    if (isNaN(this.bizuth.bungalow)) {
      this.bizuth.bungalow = this.bizuth.bungalow.id;
    }
    this.form.patchValue(this.bizuth);
  }

  submitChanges(formValues) {
    if (formValues !== undefined) {
      Object.assign(this.bizuth, this.form.value);
      this.wei.updateBizuth(this.bizuth).subscribe(b => {
        this.setBizuth(b);
      });
    }
  }

  updateMemberForBizuth(m: Member) {
    this.bizuth.member = m;
    this.setBizuth(this.bizuth);
  }

  changeParticipant(ok: boolean) {
    if (!ok) {
      this.bizuth.bungalow = null;
      this.bizuth.bus = null;
    }
    this.bizuth.participant = ok;
    this.wei.updateBizuth(this.bizuth).subscribe(b => this.setBizuth(b));
  }

  delete() {
    this.sb.open('Voules-vous vraiment effacer l\'inscription de ce primo-entrant ?', 'oui', {duration: 2500}).onAction().subscribe(
      () => {
        this.wei.deleteBizuth(this.bizuth).subscribe(b => {
          this.router.navigateByUrl('/view/' + this.bizuth.member.id);
        });
      }, cancel => {
        alert('cancel :)');
      }
    );
  }

  ajouterAuWei() {
    const id = this.activeRoute.snapshot.params['id'];
    this.wei.createBizuth(id).subscribe(
      b => {
        this.wei.getBizuth(id).subscribe(biz => this.setBizuth(biz));
      },
      err => {
        console.log(err);
      }
    );
  }

  /**
   * cette méthode ne modifie rien sur le serveur, c'est juste du code pour utiliser le composant "moyen de paiement" avec le bouton
   * enregistrer global de bizuth-edit (et on cache le bouton enregisrer du composant wei-moyen-paiement
   */
  changePaymentMethodView(payment_method: number) {
    console.log('pm', payment_method);
    this.bizuth.payment_method = payment_method;
  }

  dossierValide() {
    return dossierValide(this.bizuth);
  }
}
