import {Component, Input, OnInit} from '@angular/core';
import {Bizuth} from '../../../data/wei';

@Component({
  selector: 'app-wei-caution-info-span',
  templateUrl: './wei-caution-info-span.component.html',
  styleUrls: ['./wei-caution-info-span.component.scss']
})
export class WeiCautionInfoSpanComponent implements OnInit {
  @Input() bizuth: Bizuth;

  constructor() {
  }

  ngOnInit() {
  }

}
