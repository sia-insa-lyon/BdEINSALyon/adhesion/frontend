import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BizuthEditComponent} from './bizuth-edit.component';

describe('BizuthEditComponent', () => {
  let component: BizuthEditComponent;
  let fixture: ComponentFixture<BizuthEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BizuthEditComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BizuthEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
