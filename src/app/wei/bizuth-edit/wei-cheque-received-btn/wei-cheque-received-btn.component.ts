import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Bizuth, WeiService} from '../../../data/wei';

@Component({
  selector: 'app-wei-cheque-received-btn',
  templateUrl: './wei-cheque-received-btn.component.html',
  styleUrls: ['./wei-cheque-received-btn.component.scss']
})
export class WeiChequeReceivedBtnComponent implements OnInit {
  @Input() bizuth: Bizuth;
  @Output() updatedBizuth = new EventEmitter<Bizuth>();

  constructor(private wei: WeiService) {
  }

  ngOnInit() {
  }

  changePaiementRecuChequeWei(ok: boolean) {
    this.wei.patchChequeRecu(this.bizuth, ok).subscribe(b => this.updatedBizuth.emit(b));
  }
}
