import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-wei-remarques-regime-fields',
  templateUrl: './wei-remarques-regime-fields.component.html',
  styleUrls: ['./wei-remarques-regime-fields.component.scss']
})
export class WeiRemarquesRegimeFieldsComponent implements OnInit {
  @Input() form: FormGroup;

  constructor() {
  }

  ngOnInit() {
  }

}
