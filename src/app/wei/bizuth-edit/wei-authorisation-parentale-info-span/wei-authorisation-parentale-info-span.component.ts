import {Component, Input, OnInit} from '@angular/core';
import {Bizuth} from '../../../data/wei';

@Component({
  selector: 'app-wei-authorisation-parentale-info-span',
  templateUrl: './wei-authorisation-parentale-info-span.component.html',
  styleUrls: ['./wei-authorisation-parentale-info-span.component.scss']
})
export class WeiAuthorisationParentaleInfoSpanComponent implements OnInit {
  @Input() bizuth: Bizuth;

  constructor() {
  }

  ngOnInit() {
  }

}
