import {Component, Input, OnInit, OnChanges, SimpleChanges} from '@angular/core';
import {Bizuth, Bungalow, Bus, WeiService} from '../../../data/wei';
import {FormGroup} from '@angular/forms';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-wei-bus-bungalow-fields',
  templateUrl: './wei-bus-bungalow-fields.component.html',
  styleUrls: ['./wei-bus-bungalow-fields.component.scss']
})
export class WeiBusBungalowFieldsComponent implements OnInit {
  @Input() bizuth: Bizuth;
  @Input() form: FormGroup;
  busDispo: Bus[] = [];
  bungalowsFiltre: Bungalow[] = [];
  bungalowsDispo: Bungalow[] = [];
  bizuthBus: Bus = null;
  bizuthBungalow: Bungalow = null;

  constructor(private wei: WeiService) {
  }

  ngOnInit() {
    if (this.bizuth.bus !== null){
      forkJoin(
        this.wei.getBus(String(this.bizuth.bus)), this.wei.listBusLibres()
      ).subscribe((res) => {

        this.bizuthBus = res[0];
        this.form.get('bus').setValue(res[0].id);

        if (this.bizuthBus !== null && !res[1].find(bus => bus.id === this.bizuthBus.id)){
          this.busDispo = [...res[1], this.bizuthBus];
        } else{
          this.busDispo = res[1];
        }
      })
    } else {
      this.wei.listBusLibres().subscribe(b => {
        if (this.bizuthBus !== null && !b.find(bus => bus.id === this.bizuthBus.id)){
          this.busDispo = [...b, this.bizuthBus];
        } else{
          this.busDispo = b;
        }
      });
    }

    if (this.bizuth.bungalow !== null){
      forkJoin(
        this.wei.getBungalow(String(this.bizuth.bungalow)), this.wei.listBungalowsLibres()
      ).subscribe((res) => {

        this.bizuthBungalow = res[0]
        this.form.get('bungalow').setValue(res[0].id);

        this.bungalowsDispo = res[1]
        this.bungalowsFiltre = this.filterBungalows()
        if (this.bizuth !== null) {
          this.form.patchValue(this.bizuth);
        }

      })
    } else {
      this.wei.listBungalowsLibres().subscribe(bg => {
        this.bungalowsDispo = bg
        this.bungalowsFiltre = this.filterBungalows()
        if (this.bizuth !== null) {
          this.form.patchValue(this.bizuth);
        }
      });
    }
  }

  updateBungalowSelect(e){
    this.bungalowsFiltre = this.filterBungalows()
    if (this.bizuthBungalow !== null && this.bungalowsFiltre.find(bg => bg.id === this.bizuthBungalow.id)){
      this.form.get('bungalow').setValue(this.bizuthBungalow.id);
    } else {
      this.form.get('bungalow').setValue(null);
    }
  }

  filterBungalows() {
    let filteredBungalow: Bungalow[] = this.wei.filterBungalowForBus(this.form.get('bus').value, this.bungalowsDispo, this.bizuth);
    if (
      this.bizuthBungalow !== null
      && !filteredBungalow.find(bg => bg.id === this.bizuthBungalow.id)
      && (this.bizuthBungalow.bus===this.form.get('bus').value || this.bizuthBus===this.form.get('bus').value)
    ){
      filteredBungalow = [...filteredBungalow, this.bizuthBungalow]
    }
    return filteredBungalow;
  }
}
