import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Bizuth, WeiService} from '../../../data/wei';

@Component({
  selector: 'app-wei-caution-btn',
  templateUrl: './wei-caution-btn.component.html',
  styleUrls: ['./wei-caution-btn.component.scss']
})
export class WeiCautionBtnComponent implements OnInit {
  @Input() bizuth: Bizuth;
  @Output() updatedBizuth = new EventEmitter<Bizuth>();

  constructor(private wei: WeiService) {
  }

  ngOnInit() {
  }

  changeCaution(ok: boolean) {
    this.wei.patchCaution(this.bizuth, ok).subscribe(b => this.updatedBizuth.emit(b));
  }
}
