import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Bizuth, WeiService} from '../../../data/wei';

@Component({
  selector: 'app-wei-authorisation-parentale-btn',
  templateUrl: './wei-authorisation-parentale-btn.component.html',
  styleUrls: ['./wei-authorisation-parentale-btn.component.scss']
})
export class WeiAuthorisationParentaleBtnComponent implements OnInit {
  @Input() bizuth: Bizuth;
  @Output() updatedBizuth = new EventEmitter<Bizuth>();

  constructor(private wei: WeiService) {
  }

  ngOnInit() {
  }

  changeAutorisationparentale(ok: boolean) {
    this.wei.patchAutorisationparentale(this.bizuth, ok).subscribe(b => this.updatedBizuth.emit(b));
  }
}
