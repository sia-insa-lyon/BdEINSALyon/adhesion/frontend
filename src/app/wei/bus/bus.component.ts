import {Component, OnInit} from '@angular/core';
import {Bizuth, Bus, WeiService} from '../../data/wei';
import {UiService} from '../../data/ui.service';
import { CsvService } from '../../data/csv.service';

@Component({
  selector: 'app-bus',
  templateUrl: './bus.component.html',
  styleUrls: ['./bus.component.scss']
})
export class BusComponent implements OnInit {
  listeBus: Bus[] = [];
  showFormNew = false;

  constructor(private wei: WeiService, private ui: UiService, private csv: CsvService) {
  }

  ngOnInit() {
    this.wei.listBus().subscribe(lb => this.listeBus = lb);
  }

  new(nb: Bus) {
    this.ui.setLoading(true);
    this.wei.createBus(nb).subscribe(rb => {
      this.listeBus.push(rb);
      this.ui.setLoading(false);
      this.showFormNew = false;
    });
  }

  /* Methode: exportCsvBus()
   *
   *  Pour exporter en csv les bizs avec le numéro
   *  de leur bus
   *  avec leur nom et leur prénom.
   *  
   */
  exportCsvBus(): void {
    this.wei.getListBizBus().subscribe((allBiz: Bizuth[]) => {
      const tableauRegime: Object[] = allBiz.map(({member, est_mineur, bungalow, bus}) => ({last_name: member.last_name,first_name: member.first_name,phone: member.phone,bungalowId: bungalow?bungalow.id:'', bungalowNom: bungalow?bungalow.nom:'' ,est_mineur: est_mineur,id: bus.id, nom: bus.nom}));
      const headers = ['first_name','last_name','phone','est_mineur','bungalowId','bungalowNom','id', 'nom'];
      const columnName = ['Prénom','Nom','Téléphone','Est mineur','Numéro bungalow','Nom bungalow','Numéro du bus', 'Nom du bus'];
      const fileName = 'liste_bus_wei';
      this.csv.downloadFile(tableauRegime, headers, columnName, fileName);
    });
  }

}
