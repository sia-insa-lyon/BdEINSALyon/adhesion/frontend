import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Bus} from '../../../data/wei';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-bus-edit',
  templateUrl: './bus-edit.component.html',
  styleUrls: ['./bus-edit.component.scss']
})
export class BusEditComponent implements OnInit {
  @Input() bus: Bus;
  @Output() out = new EventEmitter<Bus>();
  form: FormGroup;

  constructor() {
    this.form = new FormGroup({
      nom: new FormControl(''),
      places: new FormControl(0),
    });
  }

  ngOnInit() {
    if (this.bus !== undefined) {
      this.form.patchValue(this.bus);
    }
  }

  save(formData) {
    this.out.emit(formData);
  }

}
