import {Component, OnInit} from '@angular/core';
import {Bizuth, Bus, WeiService} from '../../../data/wei';
import {ActivatedRoute} from '@angular/router';
import {UiService} from '../../../data/ui.service';
import { CsvService } from '../../../data/csv.service';

@Component({
  selector: 'app-detail',
  templateUrl: './bus-detail.component.html',
  styleUrls: ['./bus-detail.component.scss']
})
export class BusDetailComponent implements OnInit {
  bus: Bus;
  bizuths: Bizuth[];

  showEdit = false;

  constructor(private wei: WeiService,private csv: CsvService, private activeRoute: ActivatedRoute, private ui: UiService) {
  }

  ngOnInit() {
    const id = this.activeRoute.snapshot.params['id'];
    this.ui.setLoading(true);
    this.wei.getBus(id).subscribe(bus => {
        this.bus = bus;
        if (this.bizuths !== undefined) {
          this.ui.setLoading(false);
        }
      }
    );
    this.wei.getBusBizuths(id).subscribe(bizs => {
      this.bizuths = bizs;
      if (this.bus !== undefined) {
        this.ui.setLoading(false);
      }
    })
    ;
  }

  apply(bus: Bus) {
    this.ui.setLoading(true);
    this.wei.updateBus(this.bus.id, bus).subscribe(nb => {
      this.bus = nb;
      this.showEdit = false;
      this.ui.setLoading(false);
    });
  }

    /* Methode: exportCsvSingleBus()
   *
   *  Pour exporter en csv les bizs du bus
   *  avec leur nom et leur prénom.
   *  
   */
    exportCsvSingleBus(): void {
        const tableauRegime: Object[] = this.bizuths.map(({member,est_mineur,bungalow}) => ({last_name: member.last_name,first_name: member.first_name,phone: member.phone , est_mineur: (est_mineur)?est_mineur:null, bungalowId: bungalow}));
        const headers = ['first_name','last_name','phone','est_mineur','bungalowId','bungalowNom'];
        const columnName = ['Prénom','Nom','Téléphone','Est mineur','Numéro bungalow','Départ','A1','A2','Retour','A3','A4',`Nom du bus: ${this.bus.nom}`];
        const fileName = 'liste_bus_'+this.bus.nom;
        this.csv.downloadFile(tableauRegime, headers, columnName, fileName);
    }

}
