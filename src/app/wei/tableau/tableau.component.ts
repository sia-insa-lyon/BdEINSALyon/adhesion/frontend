import {Component, OnInit, ViewChild} from '@angular/core';
import {Bizuth, dossierValide, WeiService} from '../../data/wei';
import {FormControl, FormGroup} from '@angular/forms';
import {UiService} from '../../data/ui.service';
import {CsvService} from '../../data/csv.service';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { Router } from '@angular/router';

@Component({
  selector: "app-tableau",
  templateUrl: "./tableau.component.html",
  styleUrls: ["./tableau.component.scss"],
})
export class TableauComponent implements OnInit {
  bizuths: Bizuth[] = [];
  form: FormGroup;
  nombrePlaces: number;
  nombreParticipants: number;
  displayedColumns = [
    "numero",
    "first_name",
    "last_name",
    "dateInscription",
    "dossierValide",
    "paiementWEI",
    "paiementVA",
    "caution",
    "majeur",
    "autorisationParentale",
    "rang",
    "participant",
    "bus",
    "bungalow",
    "actions",
  ];
  public dataSource: MatTableDataSource<Bizuth>;
  public isLoading = true;


  @ViewChild(MatSort, {static: false}) sort: MatSort;
  constructor(
    private wei: WeiService,
    private ui: UiService,
    private csv: CsvService,
    private router: Router
  ) {
    this.ui.setLoading(true);
  }

  ngOnInit() {
    this.form = new FormGroup({
      search: new FormControl(""),
    });
    this.wei.listBizuths().subscribe(
      (x) => {
        this.wei.getStats().subscribe(
          (dataWei) => {
            this.nombrePlaces = dataWei["total_places"];
            this.nombreParticipants = dataWei["participants"];
            this.updateRankBizuths(this.defaultSort(x));
            this.isLoading = false;
            this.ui.setLoading(false);
          },
          (error) => console.log(error)
        );
      },
      (error) => console.log(error)
    );
  }

  defaultSort(x) {
    //Ordre de tri de base:
    // 1 . Participant premier
    // 2 . Premier inscrit jamais participant
    // 3.  Premier inscirt ancien pariticpant
    return x.sort((a, b) => {
      if (!a.participant && b.participant) return 1;
      if (a.participant && !b.participant) return -1;
      if (a.prioritaire_file && !b.prioritaire_file) return -1;
      if (!a.prioritaire_file && b.prioritaire_file) return 1;
      return a.id - b.id;
    });
  }


  search() {
    this.dataSource.filter = this.form.value.search.trim().toLocaleLowerCase();
  }

  /* Methode: updateRankBizuths()
   *
   * Permet d'obtenir le rang d'un biz
   * et sa position en file d'attente
   */
  updateRankBizuths(listBiz: Bizuth[]): void {
    listBiz.forEach((biz: Bizuth, index: number) => {
      index++;

      if (index <= this.nombrePlaces) {
        if (biz.participant) {
          // Participant
          biz.rang = ` ${index}`;
        } else {
          // Place libre mais pas participant
          biz.rang = `~ ${index}`;
        }
      } else {
        // File d'attente
        biz.rang = `+ ${index - this.nombrePlaces}`;
      }
    });
    this.bizuths = listBiz;
    this.dataSource = new MatTableDataSource(this.bizuths);
    // Define filter
    this.dataSource.filterPredicate = (data: Bizuth, filter: string) =>
    (data.member.first_name.toLowerCase().includes(filter) || data.member.last_name.toLowerCase().includes(filter));
    this.dataSource.sort = this.sort;
  }

  // METHODE DE TRI
  // On override les méthodes de sort d'Angular Material
  sortData(sort: Sort) {
    const data = this.dataSource.data.slice();
    if (!sort.active || sort.direction === '') {
      this.dataSource.data = this.defaultSort(data);
      return;
    }

    this.dataSource.data = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'numero':
          return compare(a.id, b.id, isAsc);
        case 'first_name':
          return compare(a.member.first_name, b.member.first_name, isAsc);
        case 'last_name':
          return compare(a.member.last_name, b.member.last_name, isAsc);
        case 'dateInscription':
          return compare(
            a.date_inscription,
            b.date_inscription,
            isAsc
          );
        case 'dossierValide':
          return compare(
            dossierValide(a),
            dossierValide(b),
            isAsc
          );
        case 'paiementWEI':
          return compare(
            a.paiement_recu,
            b.paiement_recu,
            isAsc
          );
        case 'caution':
          return compare(
            a.caution_valide,
            b.caution_valide,
            isAsc
          );
        case 'majeur':
          return compare(a.est_mineur, b.est_mineur, isAsc);
        case 'participant':
          return compare(a.participant, b.participant, isAsc);
        default:
          return 0;
      }
    });
  }

  dossierValide(bizuth) {
    return dossierValide(bizuth);
  }

  /* Methode: exportCsvRegime()
   *
   *  Pour exporter en csv les champs 'Régime particulier'
   *  et 'Informations particulières' de chaque biz  participant
   *  avec leur nom et leur prénom.
   *
   *  Cela permet de prévoir la nourriture pour le WEI.
   *
   */
  exportCsvRegime(): void {
    this.wei.getListBizSpeciaux().subscribe((tableauBizSpeciaux: Bizuth[]) => {
      const tableauRegime: Object[] = tableauBizSpeciaux.map(
        ({ member, regime_particulier, informations_speciales }) => ({
          last_name: member.last_name,
          first_name: member.first_name,
          regime_particulier,
          informations_speciales,
        })
      );
      const headers = [
        "first_name",
        "last_name",
        "regime_particulier",
        "informations_speciales",
      ];
      const columnName = [
        "Prénom",
        "Nom",
        "Regime particulier",
        "Informations spéciales",
      ];
      const fileName = "regime_wei";
      this.csv.downloadFile(tableauRegime, headers, columnName, fileName);
    });
  }

  /* Methode: exportCsvParticipants()
   *
   *  Pour exporter en csv les participants du WEI
   *
   */
  exportCsvParticipants(): void {
    this.wei.getListBizParticipants().subscribe((BizParticipants: Bizuth[]) => {
      const tableauParticpants: Object[] = BizParticipants.map(
        ({
           member,
           age_au_wei,
           est_mineur,
           caution_valide,
           autorisation_parentale_valide,
           bus,
           bungalow,
         }) => ({
          last_name: member.last_name,
          first_name: member.first_name,
          gender: member.gender,
          phone: member.phone,
          email: member.email,
          age_au_wei,
          est_mineur,
          caution_valide,
          // Si mineur, on affiche l'autorisation
          autorisation_parentale_valide: est_mineur
              ? autorisation_parentale_valide
              : "",
          bus: bus ? bus.id : "",
          bungalow: bungalow ? bungalow.id : "",
        })
      );
      const headers = [
        "first_name",
        "last_name",
        "gender",
        "phone",
        "email",
        "age_au_wei",
        "est_mineur",
        "caution_valide",
        "autorisation_parentale_valide",
        "bus",
        "bungalow",
      ];
      const columnName = [
        "Prénom",
        "Nom",
        "Genre",
        "Téléphone",
        "Email",
        "Âge au moment du WEI",
        "Est mineur",
        "Caution valide",
        "Autorisation parentale valide",
        "Numéro de bus",
        "Numéro de bungalow",
      ];
      const fileName = "particpants_wei";
      this.csv.downloadFile(tableauParticpants, headers, columnName, fileName);
    });
  }

  openBizuthPage(bizuth: Bizuth){
    this.router.navigate(['/wei/bizuth/',bizuth.member.id]);
  }
}


function compare(a: any, b: any, isAsc: boolean): number {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
