import {Component, OnInit} from '@angular/core';
import {Bizuth, Bungalow, Bus, WeiService} from '../../../data/wei';
import {UiService} from '../../../data/ui.service';
import {ActivatedRoute} from '@angular/router';
import { CsvService } from '../../../data/csv.service';

@Component({
  selector: 'app-bungalow-detail',
  templateUrl: './bungalow-detail.component.html',
  styleUrls: ['./bungalow-detail.component.scss']
})
export class BungalowDetailComponent implements OnInit {
  bungalow: Bungalow;
  bizuths: Bizuth[];
  showEdit = false;

  constructor(private wei: WeiService, private ui: UiService, private activeRoute: ActivatedRoute, private csv: CsvService) {
  }

  ngOnInit() {
    const id = this.activeRoute.snapshot.params['id'];
    this.ui.setLoading(true);
    this.wei.getBungalow(id).subscribe(bu => this.bungalow = bu);
    this.wei.getBungalowBizuths(id).subscribe(bizs => {
      this.bizuths = bizs;
      this.ui.setLoading(false);
    });
  }

  apply(nb: Bungalow) {
    console.log('nb', nb);
    this.ui.setLoading(true);
    this.wei.updateBungalow(this.bungalow.id, nb).subscribe(
      rb => {
        this.ui.setLoading(false);
        this.bungalow = rb;
        this.showEdit = false;
      }
    );
  }

  /* Methode: exportCsvSingleBungalow()
   *
   *  Pour exporter en csv les bizs du bungalow
   *  avec leur nom et leur prénom.
   *  
   */
    exportCsvSingleBungalow(): void {
      const tableauRegime: Object[] = this.bizuths.map(({member, bus, est_mineur}) => ({last_name: member.last_name,first_name: member.first_name,est_mineur: est_mineur, phone: member.phone, id: bus}));
      const headers = ['first_name','last_name','est_mineur','phone','id'];
      const columnName = ['Prénom','Nom','Est mineur','Téléphone','Numéro du bus',`Nom du bungalow: ${this.bungalow.nom}`];
      const fileName = 'liste_bungalow_'+this.bungalow.nom;
      this.csv.downloadFile(tableauRegime, headers, columnName, fileName);
  }  
}
