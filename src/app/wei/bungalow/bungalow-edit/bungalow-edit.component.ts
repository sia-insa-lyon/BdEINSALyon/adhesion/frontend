import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Bungalow, Bus, WeiService} from '../../../data/wei';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-bungalow-edit',
  templateUrl: './bungalow-edit.component.html',
  styleUrls: ['./bungalow-edit.component.scss']
})
export class BungalowEditComponent implements OnInit {
  @Input() bungalow: Bungalow;
  @Output() out = new EventEmitter<Bungalow>();
  form: FormGroup;

  listeBus: Bus[];

  constructor(private wei: WeiService) {
    this.form = new FormGroup({
      nom: new FormControl(''),
      places: new FormControl(''),
      bus: new FormControl(0),
      genre: new FormControl('W'),
    });
  }

  ngOnInit() {
    this.wei.listBus().subscribe(lb => {
      this.listeBus = lb;
      if (this.bungalow !== undefined) {
        this.form.patchValue(this.bungalow);
      }
    });
  }

  save(formData) {
    this.out.emit(formData);
  }

}
