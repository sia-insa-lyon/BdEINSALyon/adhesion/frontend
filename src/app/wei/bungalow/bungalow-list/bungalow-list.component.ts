import {Component, OnInit} from '@angular/core';
import {Bizuth, Bungalow, WeiService} from '../../../data/wei';
import {UiService} from '../../../data/ui.service';
import { CsvService } from '../../../data/csv.service';

@Component({
  selector: 'app-bungalow-list',
  templateUrl: './bungalow-list.component.html',
  styleUrls: ['./bungalow-list.component.scss']
})
export class BungalowListComponent implements OnInit {
  bungalows: Bungalow[];

  showFormNew = false;

  constructor(private wei: WeiService, private ui: UiService, private csv: CsvService) {
  }

  ngOnInit() {
    this.ui.setLoading(true);
    this.wei.listBungalows().subscribe(lb => {
      this.bungalows = lb;
      this.bungalows.sort((a, b) => {
        if (a.bus > b.bus) {
          return 1;
        }
        if (b.bus > a.bus) {
          return -1;
        }
        if (a.bus === b.bus) {
          return (a.genre === 'W' ? -1 : 1);
        }
      });
      this.ui.setLoading(false);
    });
  }

  new(nb: Bungalow) {
    this.ui.setLoading(true);
    this.wei.createBungalow(nb).subscribe(rb => {
      this.ui.setLoading(false);
      this.showFormNew = false;
      this.bungalows.push(nb);
    });
  }


  /* Methode: exportCsvBungalow()
   *
   *  Pour exporter en csv les bizs (nom et prenom) avec le numéro
   *  de leur bungalow
   *  
   */
  exportCsvBungalow(): void {
      this.wei.getListBizBungalow().subscribe((allBiz: Bizuth[]) => {
        const tableauRegime: Object[] = allBiz.map(({member, bungalow,bus, est_mineur}) => ({last_name: member.last_name,first_name: member.first_name,phone: member.phone,est_mineur: est_mineur, bus: bus.id,genre: bungalow.genre, nom: bungalow.nom}));
        const headers = ['first_name','last_name','phone','est_mineur','genre','bus','nom'];
        const columnName = ['Prénom','Nom','Téléphone','Est mineur','Genre','Numéro du bus', 'Nom du bungalow'];
        const fileName = 'liste_bungalow_wei';
        this.csv.downloadFile(tableauRegime, headers, columnName, fileName);
      });
    }
}
