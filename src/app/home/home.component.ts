import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UiService} from '../data/ui.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  // Vérification de l'état de WEI
  // Vérification de l'authentification/etat adhésion
  oauthError: string;

  // Vérification de l'état de paiements

  constructor(private route: ActivatedRoute, private ui: UiService) {
  }

  ngOnInit() {
    this.oauthError = this.route.snapshot.queryParamMap.get('error');
    this.ui.setTitle('Adhésion');
  }

}

