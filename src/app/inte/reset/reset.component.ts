import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class ResetComponent implements OnInit {
  newUrl: string;

  constructor(private router: Router, private ar: ActivatedRoute) {
  }

  ngOnInit() {
    this.newUrl = this.ar.snapshot.paramMap.get('url');
    this.router.navigateByUrl(this.newUrl);
  }

}
