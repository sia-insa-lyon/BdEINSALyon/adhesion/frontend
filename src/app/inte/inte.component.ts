import {Component, OnInit} from '@angular/core';
import {CardService, Member, Membership, MembershipService, MembershipType, MembersService, method_to_str} from '../data/members.service';
import {UiService} from '../data/ui.service';
import {Bizuth, WeiService} from '../data/wei';
import {FormControl, FormGroup} from '@angular/forms';
import {LoggerService} from '../data/management.service';

@Component({
  selector: 'app-inte',
  templateUrl: './inte.component.html',
  styleUrls: ['./inte.component.scss']
})
export class InteComponent implements OnInit {
  member: Member;
  bizuth: Bizuth;
  membership: Membership;
  membershipType: MembershipType;
  moyenPaiement: string;
  isLoading = false;
  form: FormGroup;

  hideWei = false;


  constructor(public ui: UiService,
              private service: MembersService,
              private ms: MembershipService,
              private cardService: CardService,
              private wei: WeiService,
              private logger: LoggerService) {
  }

  ngOnInit() {
    this.hideWei = true;

    this.ui.setTitle('Chaîne d\'inté');
    // this.service.retrieve(4).subscribe(x=>this.member=x);
    this.form = new FormGroup({
      regime_particulier: new FormControl(''),
      informations_speciales: new FormControl(''),
    });
  }

  ngOnDestroy() {
    this.ui.clearTitle();
  }

  holdMember(event: Member) {
    this.member = event;
    this.refreshBizuth(event.id);
    this.ui.setLoading(false);
    this.logger.log('inte_hold_member', event);

  }

  refreshBizuth(id: number) {
    this.wei.getBizuth(id.toString()).subscribe(b => {
      this.bizuth = b;
      this.form.patchValue(this.bizuth);
    });
  }

  refreshMember(id: number) {
    this.service.retrieve(id).subscribe(member => {
      this.member = member;
      if (member.category == 'student') {
        if (member.student_profile.study_year === '1A' && member.student_profile.school === 'INSA') {
          this.refreshBizuth(id);
        }
      }
    });
  }

  createBizuth() {
    this.wei.createBizuth(this.member.id).subscribe(_ => this.refreshBizuth(this.member.id));
  }

  submitBizuthChanges(formValues) {
    if (formValues !== undefined) {
      Object.assign(this.bizuth, this.form.value);
      this.wei.patchRegimeEtInformations(this.bizuth).subscribe(_ => this.refreshBizuth(this.member.id));
    }
  }

  holdMembership(ev: Membership) {
    this.membership = ev;
    this.moyenPaiement = method_to_str(ev.payment_method);
    this.service.retrieve(this.membership.member).subscribe(x => this.member = x);
  }

  holdMembershipType(ev: MembershipType) {
    this.membershipType = ev;
  }

  updateMember() {
    this.service.retrieve(this.membership.member).subscribe(x => this.member = x);
  }

  changeHideWei(value) {
    window.localStorage.setItem('hideWei', value);
  }
}
