import {Component, OnInit} from '@angular/core';
import {Member} from '../../data/members.service';
import {UiService} from '../../data/ui.service';
import {WeiService} from '../../data/wei';

@Component({
  selector: 'app-preenregistrement',
  templateUrl: './preenregistrement.component.html',
  styleUrls: ['./preenregistrement.component.scss']
})
export class PreenregistrementComponent implements OnInit {
  member: Member;
  demandeWei = true;

  constructor(private ui: UiService, private wei: WeiService) {
  }

  ngOnInit() {
    this.ui.setTitle('Pré-enregistrement');
  }

  holdMember(event: Member) {
    console.log('received event member');
    console.log(event);
    this.member = event;


    this.wei.getBizuth('' + event.id).subscribe(biz => {
      this.ui.setLoading(false);
      this.demandeWei = true;
    }, err => {
      console.log(err);
      if (err.status === 404) {
        this.demandeWei = false;
      }
      this.ui.setLoading(false);
    });
  }
}
