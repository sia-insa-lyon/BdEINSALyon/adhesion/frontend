import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-erreur',
  templateUrl: './erreur.component.html',
  styleUrls: ['./erreur.component.scss']
})
export class ErreurComponent implements OnInit {

  @Input() erreur;

  constructor() {
  }

  ngOnInit() {
    console.log(this.erreur);
  }

}
