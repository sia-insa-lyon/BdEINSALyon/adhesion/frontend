import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-chat-message-popup',
  templateUrl: './chat-message-popup.component.html',
  styleUrls: ['./chat-message-popup.component.css']
})
export class ChatMessagePopupComponent implements OnInit {
  time = 5;
  timeStep: number;
  progress = 100;

  constructor(
    public dialogRef: MatDialogRef<ChatMessagePopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string) {
    this.timeStep = this.time / 100;
  }

  ngOnInit(): void {
    console.log('init dialog');

    const timer = setInterval(() => {
      this.progress = this.progress - 1;
      if (this.progress <= 0) {
        window.clearInterval(timer);
      }
    }, 1000 * this.timeStep);
  }


}
