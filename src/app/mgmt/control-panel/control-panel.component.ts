import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {ChatMessageEvent, ControlPanelService} from '../../data/management.service';
import {UiService} from '../../data/ui.service';

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.css']
})
export class ControlPanelComponent implements OnInit {
  btnEnabled = false;
  messageForm: FormGroup;
  webConfigForm: FormGroup;

  constructor(private cps: ControlPanelService, private ui: UiService) {
    this.messageForm = new FormGroup({
      type: new FormControl('snackbar'),
      text: new FormControl(''),
    });

    this.webConfigForm = new FormGroup({
      admin_frontend_enabled: new FormControl(true),
      public_frontend_enabled: new FormControl(true),
      message: new FormControl(''),
    });
  }

  ngOnInit(): void {
    this.ui.setLoading(true);
    this.cps.getWebConfig().subscribe((wc) => {
      this.webConfigForm.setValue(wc);
      this.btnEnabled = true;
      this.ui.setLoading(false);
    });
    this.ui.setTitle('Management');
  }

  sendMessage(formdata) {
    this.ui.setLoading(true);
    this.cps.broadcast(new ChatMessageEvent(formdata.type, formdata.text)).subscribe(
      () => this.ui.setLoading(false),
      () => this.ui.setLoading(false)
    );
  }

  changeConfig(formData) {
    this.btnEnabled = false;
    this.ui.setLoading(true);
    this.cps.changeWebConfig(formData).subscribe((nwc) => {
      this.webConfigForm.setValue(nwc);
      this.ui.setLoading(false);
      this.btnEnabled = true;
      /*this.webConfigForm.admin_frontend_enabled = nwc.admin_frontend_enabled
      this.webConfigForm.public_frontend_enable = nwc.public_frontend_enable*/
    }, () => {
      this.ui.setLoading(false);
      this.btnEnabled = true;
    });
  }
}
