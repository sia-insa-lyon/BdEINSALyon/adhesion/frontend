import {Component, Input, OnInit} from '@angular/core';
import {CDPState} from '../../data/management.service';

@Component({
  selector: 'app-cdp-detail',
  templateUrl: './cdp-detail.component.html',
  styleUrls: ['./cdp-detail.component.css']
})
export class CdpDetailComponent implements OnInit {
  /*_cdp: CDPState;
  @Input()
  get cdp() {
    return this._cdp;
  }

  set cdp(cdp: CDPState) {
    this._cdp = cdp;
  }*/
  @Input() cdp: CDPState;

  constructor() {
  }

  ngOnInit(): void {
  }
}
