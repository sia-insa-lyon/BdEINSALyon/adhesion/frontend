import {Component, OnInit} from '@angular/core';
import {KeycloakService} from 'keycloak-angular';
import {CDPState, DISCONNECTED, INFO, ROUTE_CHANGE, WatcherTowerService, WatchEvent} from '../../data/management.service';
import {UiService} from '../../data/ui.service';


@Component({
  selector: 'app-watcher',
  templateUrl: './watch-tower.component.html',
  styleUrls: ['./watch-tower.component.css']
})
export class WatchTowerComponent implements OnInit {
  logEvents: WatchEvent[] = [];
  cdps: Map<string, CDPState> = new Map<string, CDPState>();

  constructor(private kcs: KeycloakService,
              private watcher: WatcherTowerService,
              private ui: UiService) {
  }

  ngOnInit(): void {
    this.kcs.getToken().then((jwt) => {
      this.watcher.init(jwt);
      this.watcher.subscribe((ev) => this.onMessage(ev));
    });
    this.ui.setTitle('WatchTower');
  }

  onMessage(msg: WatchEvent) {
    console.log(msg);
    if (msg.type !== INFO && msg.type !== ROUTE_CHANGE) {
      this.logEvents.push(msg);
    }
    if (msg.user) {
      let cdp = this.cdps.get(msg.user);
      if (cdp === undefined) {
        cdp = new CDPState(msg.user);
        this.cdps.set(msg.user, cdp);
      }
      /*if (msg.type === CONNECTED) {
      }*/
      if (msg.type === DISCONNECTED) {
        this.cdps.delete(msg.user);
      }
      if (msg.type === ROUTE_CHANGE) {
        cdp.route = msg.data;
      }
    }
  }

}
