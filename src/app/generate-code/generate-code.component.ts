import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {CardService} from '../data/members.service';

// MAT-TABLE DATA
export interface CarteVA {
  numero: number;
  code: string;
}


@Component({
  selector: 'app-generate-code',
  templateUrl: './generate-code.component.html',
  styleUrls: ['./generate-code.component.scss']
})
export class GenerateCodeComponent implements OnInit {


  public dataSource: MatTableDataSource<CarteVA>;

  // MAT-TABLE SETTINGS
  displayedColumns: string[] = ['numero', 'code'];

  constructor(
    private cardService: CardService
  ) {
  }

  ngOnInit(): void {

  }

  /* Methode: generateCode
  Génère N code-barre de carte VA
  */
  generateCode(nb: number): void {
    this.cardService.generate(nb).subscribe((res: string[]) => {
      this.createDataSource(res);
    });
  }


  createDataSource(listeCode: string[]): void {
    const data = listeCode.map((code, index) => {
      return {numero: index, code: code};
    });
    this.dataSource = new MatTableDataSource<CarteVA>(data);
  }
}
