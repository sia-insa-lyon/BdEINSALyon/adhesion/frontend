import {Injectable} from '@angular/core';
import {webSocket} from 'rxjs/webSocket';
import {environment} from '../../environments/environment';

export class CompanionMessage {
  code: string;
}

@Injectable({
  providedIn: 'root'
})
export class CompanionService {


  public wsIdentifierToken = CompanionService.generateId();
  chaussetteInternet = webSocket(environment.mobile_gateway_url + '/admin/' + this.wsIdentifierToken);
  // chaussetteInternet = webSocket('ws://localhost:8000/admin/' + sessionStorage.getItem('access_token'));
  observableSync = this.chaussetteInternet.asObservable();

  // chaussetteInternet = webSocket(environment.mobile_gateway_url + '/admin/' + sessionStorage.getItem('access_token'));

  constructor() {
  }

  static generateId() {
    return Math.random().toString(36).substring(2, 12)
      + Math.random().toString(36).substring(2, 12) +
      Math.random().toString(36).substring(2, 12);
  }

  testsend() {
    return this.chaussetteInternet.next('envoi depuis Angular');
  }

}
