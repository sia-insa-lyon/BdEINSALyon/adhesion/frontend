import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {Member, Membership} from './members.service';

export class TransactionMercanet {
  id: number;
  issuer: Member;
  membership: Membership;
  transactionReference: string;
  amount: number;
  returnContext: string;
  normalReturnUrl: string;
  transactionDateTime: Date;
  responseCode: string;
  created_at: Date;
  updated_at: Date;
  status: string;
}

@Injectable()
export class MercanetService {
  constructor(private http: HttpClient) {
  }

  list(): Observable<TransactionMercanet[]> {
    return this.http.get<TransactionMercanet[]>(environment.adhesion_url + '/mercanet/admin/');
  }

  filter(filters: any): Observable<TransactionMercanet[]> {
    return this.http.get<TransactionMercanet[]>(environment.adhesion_url + '/mercanet/admin/', {params: new HttpParams({fromObject: filters})});

  }

  search(query: string): Observable<TransactionMercanet[]> {
    return this.http.get<TransactionMercanet[]>(environment.adhesion_url + '/mercanet/admin/?search=' + query);
  }

}
