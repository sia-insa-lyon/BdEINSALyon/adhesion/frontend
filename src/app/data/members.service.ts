import {map} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';

export function method_to_str(pm) {
  if (pm === 1) {
    return 'cb';
  }
  if (pm === 2) {
    return 'especes';
  }
  if (pm === 3) {
    return 'cheque';
  }
  if (pm === 4) {
    return 'internet';
  }
}

export class StudentProfile {
  id: number;
  study_year: string;
  department: string;
  school: string;
  student_number: string;
}

export class Membership {
  id: number;
  member: number;
  membership: string;
  valid: boolean;
  payment_method: number;
  created_at: Date;
  created_by: string;
}

export class Member {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  phone: string;
  gender: string;
  birthday: string;
  has_valid_membership: boolean;
  has_valid_card: boolean;
  student_profile?: StudentProfile | null;
  category: string;
  memberships: Membership[];
  cards: Card[];
  va_cheque_received: boolean;
}

export class SearchResult {
  count: number;
  page: number;
  pages: number;
  page_size: number;
  next: string;
  previous: string;
  results: Member[];

  constructor(data) {
    this.count = data.count;
    this.page = data.page;
    this.pages = data.pages;
    this.page_size = data.page_size;
    this.next = data.next;
    this.previous = data.previous;
    this.results = data.results;
  }

  get hasNext() {
    return this.next !== null;
  }

  get hasPrevious() {
    return this.previous !== null;
  }
}

@Injectable()
export class MembersService {

  constructor(private http: HttpClient) {
  }

  search(text: string, page: number = 1, params = {}): Observable<SearchResult> {
    return this.http.get<SearchResult>(
      environment.adhesion_api_url + '/members/',
      {params: new HttpParams({fromObject: Object.assign({page: page.toString(), search: text}, params)})}
    ).pipe(map(
      (result: any) => {
        return new SearchResult(result);
      }
    ));
  }

  retrieve(id: number): Observable<Member> {
    return this.http.get<Member>(environment.adhesion_api_url + '/members/' + id + '/');
  }

  update(member: Member): Observable<Member> {
    return this.http.put<Member>(environment.adhesion_api_url + '/members/' + member.id + '/', member);
  }

  create(member: Member): Observable<Member> {
    return this.http.post<Member>(environment.adhesion_api_url + '/members/', member);
  }

  list_last(number: number): Observable<Member[]> {
    return this.http.get<Member[]>(environment.adhesion_api_url + '/members/?ordering=-id&limit=' + number);
  }

  delete(id: number): Observable<Member> {
    return this.http.delete<Member>(environment.adhesion_api_url + '/members/' + id + '/');
  }

  patch_VA_cheque_received(member: Member, ok: boolean): Observable<Member> {
    return this.http.patch<Member>(`${environment.adhesion_api_url}/members/${member.id}/`, {
      va_cheque_received: ok,
      student_profile: member.student_profile
    });
  }

}

/*export class MembershipSearchResult {
  count: number;
  next: string;
  previous: string;
  results: Membership[];

  constructor(data) {
    this.count = data.count;
    this.next = data.next;
    this.previous = data.previous;
    this.results = data.results;
  }

  get hasNext() {
    return this.next !== null;
  }

  get hasPrevious() {
    return this.previous !== null;
  }
}*/
export class MembershipCondition {
  id: number;
  description: string;
  type: string;
  value: number;
  negation: boolean;
}

export class MembershipType {
  id: number;
  name: string;
  description: string;
  price_ht: number;
  price_ttc: number;
  enabled: boolean;
  not_valid_before: string;
  not_valid_after: string;
  conditions: MembershipCondition[];
}

export class MembershipComptaElement {
  type: MembershipType | null;
  count = 0;
  cb = 0;
  espece = 0;
  cheque = 0;
  int = 0;
}

export class FeuilleCaisseMemberships {
  total: MembershipComptaElement;
  par_types: MembershipComptaElement[];
}

@Injectable()
export class MembershipService {
  constructor(private http: HttpClient) {
  }

  /**
   * On peut inclure une condition à la création visiblement
   * cependant ça serait plus simple de le faire juste depuis l'admin Django
   */
  create(data: Membership): Observable<Membership> {
    return this.http.post<Membership>(environment.adhesion_api_url + '/memberships/', data);
  }

  update(data: Membership): Observable<Membership> {
    return this.http.put<Membership>(environment.adhesion_api_url + '/memberships/' + data.id + '/', data);
  }

  get(id: number): Observable<Membership> {
    return this.http.get<Membership>(environment.adhesion_api_url + '/memberships/' + id + '/');
  }

  listTypes(): Observable<MembershipType[]> {
    return this.http.get<MembershipType[]>(environment.adhesion_api_url + '/membership_types/');
  }

  me(): Observable<Member> {
    return this.http.get<Member>(environment.adhesion_url + '/me');
  }

  filterTypes(idmember: number): Observable<MembershipType[]> {
    return this.http.get<MembershipType[]>(environment.adhesion_api_url + '/membership_types/?member=' + idmember);
  }

  feuilleCaisse(start_datetime: Date, stop_datetime: Date): Observable<FeuilleCaisseMemberships> {
    return this.http.post<FeuilleCaisseMemberships>(environment.adhesion_api_url + '/feuille_caisse/', {
      start_datetime: start_datetime,
      stop_datetime: stop_datetime,
    });
  }

  feuilleCaisseToday(): Observable<FeuilleCaisseMemberships> {
    return this.http.get<FeuilleCaisseMemberships>(environment.adhesion_api_url + '/feuille_caisse/');
  }

  delete(id: number): Observable<Membership> {
    return this.http.delete<Membership>(environment.adhesion_api_url + '/memberships/' + id + '/');
  }
}

export class Card {
  id: number;
  code: string;
  member: number;
  activated: boolean;
  valid_member: boolean;
}

@Injectable()
export class CardService {
  constructor(private http: HttpClient) {

  }

  create(data: Card) {
    return this.http.post<Card>(environment.adhesion_api_url + '/cards/', data);
  }

  update(data: Card) {
    return this.http.put<Card>(environment.adhesion_api_url + '/cards/' + data.code + '/', data);
  }

  getMember(code: string): Observable<Member> {
    return this.http.get<Member>(
      environment.adhesion_api_url + '/cards/' + code + '/member');
  }

  getUsername(): Observable<any> {
    return this.http.get(environment.adhesion_url + '/me');
  }

  generate(numCards: number): Observable<string[]> {
    return this.http.get<string[]>(`${environment.adhesion_api_url}/cards/generate_cards?num_cards=${numCards}`);
  }
}
