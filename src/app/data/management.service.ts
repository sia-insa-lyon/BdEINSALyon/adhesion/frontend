import {Injectable} from '@angular/core';
import {webSocket, WebSocketSubject} from 'rxjs/webSocket';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

export const ROUTE_CHANGE = 'route_change';
export const M_E_B = 'member_edit_before';
export const M_E_A = 'member_edit_after';
export const M_C = 'member_create';
export const CONNECTED = 'connected';
export const DISCONNECTED = 'disconnected';
export const INFO = 'info';

export const base_websocket = environment.mgmt_websocket;
export const base_endpoint = environment.mgmt_endpoint;

export class LogEvent {
  type: string;
  data: any;
}

export class WatchEvent {
  type: string;
  data: any;
  user: string;
}

export class CDPState {
  username: string;
  route: string;

  constructor(username) {
    this.username = username;
    this.route = '/';
  }
}

export class ChatMessageEvent {
  static MODAL = 'modal';
  static SNACKBAR = 'snackbar';
  type: string;
  data: string;

  constructor(type, text) {
    this.data = text;
    this.type = type;
  }
}

@Injectable({
  providedIn: 'root'
})
export class LoggerService {
  private static toileChaussette: WebSocketSubject<any> = null;
  public websocket: WebSocketSubject<unknown>;

  constructor() {
    if (LoggerService.toileChaussette === null) {
      this.websocket = webSocket(base_websocket + '/logger');
    } else {
      this.websocket = LoggerService.toileChaussette;
    }
  }

  subscribe(callback) {
    this.websocket.asObservable().subscribe(callback);
  }

  init(jwt: string) { // appelé dans AppComponent
    if (LoggerService.toileChaussette === null) {
      this.websocket.next(jwt);
      LoggerService.toileChaussette = this.websocket;
    }
  }

  log(type: string, data: any) {
    this.websocket.next({
      type: type,
      data: data
    });
  }
}

@Injectable({
  providedIn: 'root'
})
export class WatcherTowerService {
  private static websocket: WebSocketSubject<WatchEvent> = null;

  init(jwt: string) {
    if (WatcherTowerService.websocket === null) {
      WatcherTowerService.websocket = webSocket('ws://localhost:8090/watch');
      // @ts-ignore
      WatcherTowerService.websocket.next(jwt);
    }
  }

  subscribe(callback) {
    WatcherTowerService.websocket.asObservable().subscribe(callback);
  }
}

export class WebConfig {
  admin_frontend_enabled: boolean;
  public_frontend_enable: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class ControlPanelService {
  constructor(private http: HttpClient) {
  }

  broadcast(msg: ChatMessageEvent): Observable<void> {
    return this.http.post<void>(base_endpoint + '/protected/broadcast', msg);
  }

  changeWebConfig(data: WebConfig): Observable<WebConfig> {
    return this.http.post<WebConfig>(base_endpoint + '/protected/webconfig', data);
  }

  getWebConfig(): Observable<WebConfig> {
    return this.http.get<WebConfig>(base_endpoint + '/webconfig');
  }
}
