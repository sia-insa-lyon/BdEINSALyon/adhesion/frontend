import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

import {Observable} from 'rxjs';

export class StudySchool {
  id: number;
  name: string;
  short_name: string;
}

export class StudyYear {
  id: number;
  name: string;
  year: number;
}

export class StudyDepartment {
  id: number;
  name: string;
  short_name: string;
  school: string;
  study_years: number[];
}

@Injectable()
export class MetadataService {

  constructor(private http: HttpClient) {
  }

  getSchools(): Observable<StudySchool[]> {
    return this.http.get<StudySchool[]>(environment.adhesion_api_url + '/study_school/');
  }

  getYears(): Observable<StudyYear[]> {
    return this.http.get<StudyYear[]>(environment.adhesion_api_url + '/study_years/');
  }

  getDepartments(): Observable<StudyDepartment[]> {
    return this.http.get<StudyDepartment[]>(environment.adhesion_api_url + '/study_department/');
  }

}
