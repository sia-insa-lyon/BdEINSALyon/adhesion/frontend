import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {Member} from './members.service';
import {map, tap} from 'rxjs/operators';

export class Bus {
  id: number;
  nom: string;
  places: number;
  places_occupees: number;
  places_restantes: number;
}

export class Bungalow {
  id: number;
  nom: string;
  bus: number;
  genre: string;
  places: number;
  places_occupees: number;
  places_restantes: number;
}

export class Bizuth {
  id: number;
  member: Member;
  est_mineur: boolean;
  regime_particulier: string;
  informations_speciales: string;
  decharge_parentale: string; // url d'un fichier à télécharer, pour l'année prochaine
  caution_valide: boolean;
  autorisation_parentale_valide: boolean;
  refunded: boolean;
  participant: boolean;
  date_inscription: string;
  prioritaire_file: boolean;
  paiement_recu: boolean;
  rang: string;
  bus: any;
  bungalow: any;
  age_au_wei: string;
  payment_method: number; // pareil que pour les adhésions
}

export class InfosWei {
  places_wei: number;
  date_wei: Date;
}

export function dossierValide(bizuth: Bizuth): boolean {
  return bizuth.caution_valide && bizuth.paiement_recu && (bizuth.member.va_cheque_received || bizuth.member.has_valid_membership)
    && (bizuth.est_mineur ? bizuth.autorisation_parentale_valide : true);
}

@Injectable()
export class WeiService {
  static listeBus: Bus[] = null;
  static listeBungalow: Bungalow[] = null;
  // static stats = null; // en fait le cache est dirigé par Django

  base = environment.adhesion_url + '/wei/admin/';
  K;

  constructor(private http: HttpClient) {
  }

  getBus(id: string): Observable<Bus> {
    return this.http.get<Bus>(this.base + 'bus/' + id + '/');
  }

  listBus(): Observable<Bus[]> {
    if (WeiService.listeBus === null) {
      console.log('listBus', 'fetched');
      return this.http.get<Bus[]>(this.base + 'bus/').pipe(tap(value => {
        WeiService.listeBus = value;
      }));
    } else {
      console.log('listBus', 'cached');
      return new Observable<Bus[]>((observer) => {
        observer.next(WeiService.listeBus);
        observer.complete();
      });
    }
  }

  updateBus(id: number, data: any): Observable<Bus> {
    WeiService.listeBus = null;
    return this.http.patch<Bus>(this.base + 'bus/' + id + '/', data);
  }

  getBusBizuths(id: string): Observable<Bizuth[]> {
    return this.http.get<Bizuth[]>(this.base + 'bus/' + id + '/bizuths/');
  }

  listBusLibres(): Observable<Bus[]> {
    return this.listBus().pipe(map(buss => buss.filter((bus, i) => bus.places_restantes > 0)));
  }

  listBungalowsLibres(): Observable<Bungalow[]> {
    return this.listBungalows().pipe(map(bungalows => bungalows.filter((b) => b.places_restantes > 0)));
  }

  createBus(data: Bus): Observable<Bus> {
    WeiService.listeBus = null;
    return this.http.post<Bus>(this.base + 'bus/', data);
  }

  getBungalow(id: string): Observable<Bungalow> {
    return this.http.get<Bungalow>(this.base + 'bungalow/' + id + '/');
  }

  getBungalowBizuths(id: string): Observable<Bizuth[]> {
    return this.http.get<Bizuth[]>(this.base + 'bungalow/' + id + '/bizuths/');
  }

  listBungalows(): Observable<Bungalow[]> {
    if (WeiService.listeBungalow === null) {
      console.log('listBugalows', 'fetched');
      return this.http.get<Bungalow[]>(this.base + 'bungalow/').pipe(tap((bungalows) => {
        WeiService.listeBungalow = bungalows;
      }));
    } else {
      console.log('listBugalows', 'cached');
      return new Observable<Bungalow[]>((observer) => {
        observer.next(WeiService.listeBungalow);
        observer.complete();
      });
    }
  }

  updateBungalow(id: number, data: Bungalow): Observable<Bungalow> {
    WeiService.listeBungalow = null;
    return this.http.patch<Bungalow>(this.base + 'bungalow/' + id + '/', data);
  }

  listBizuths(): Observable<Bizuth[]> {
    return this.http.get<Bizuth[]>(this.base + 'bizuths/' + 'tableauBizuths');
  }

  searchBizuths(query: string): Observable<Bizuth[]> {
    return this.http.get<Bizuth[]>(this.base + 'bizuths/?search=' + query);
  }

  filterSearchBizuths(filters: any): Observable<Bizuth[]> {
    return this.http.get<Bizuth[]>(this.base + 'bizuths/', {params: new HttpParams({fromObject: filters})});
  }

  createBizuth(adherent_id: number): Observable<Bizuth> {
    return this.http.post<Bizuth>(this.base + 'bizuths/', {
      member: adherent_id,
    });
  }

  getBizuth(id: string): Observable<Bizuth> {
    return this.http.get<Bizuth>(this.base + 'bizuths/' + id + '/');
  }

  updateBizuth(bizuth: Bizuth): Observable<Bizuth> {
    return this.http.patch<Bizuth>(this.base + 'bizuths/' + bizuth.member.id + '/', bizuth);
  }

  deleteBizuth(biz: Bizuth): Observable<Bizuth> {
    return this.http.delete<Bizuth>(this.base + 'bizuths/' + biz.member.id + '/');
  }

  createBungalow(data: Bungalow): Observable<Bungalow> {
    WeiService.listeBungalow = null;
    return this.http.post<Bungalow>(this.base + 'bungalow/', data);
  }

  getStats() {
    return this.http.get(this.base + 'stats/');
    /*
    if (WeiService.stats === null) {
      return this.http.get(this.base + 'stats/').pipe(tap(stats => {
        WeiService.stats = stats;
      }));
    } else {
      return new Observable<Object>((obs) => {
        obs.next(WeiService.stats);
      });
    }*/
  }

  patchCaution(bizuth: Bizuth, caution_valide: boolean): Observable<Bizuth> {
    return this.http.patch<Bizuth>(this.base + 'bizuths/' + bizuth.member.id + '/', {caution_valide: caution_valide});
  }

  patchAutorisationparentale(bizuth: Bizuth, autorisation_parentale_valide: boolean): Observable<Bizuth> {
    return this.http.patch<Bizuth>(this.base + 'bizuths/' + bizuth.member.id + '/', {
      autorisation_parentale_valide: autorisation_parentale_valide
    });
  }

  patchRegimeEtInformations(bizuth: Bizuth): Observable<Bizuth> {
    return this.http.patch<Bizuth>(this.base + 'bizuths/' + bizuth.member.id + '/', {
      informations_speciales: bizuth.informations_speciales,
      regime_particulier: bizuth.regime_particulier
    });
  }

  patchChequeRecu(bizuth: Bizuth, ok: boolean): Observable<Bizuth> {
    return this.http.patch<Bizuth>(this.base + 'bizuths/' + bizuth.member.id + '/', {
      paiement_recu: ok,
    });
  }

  patchParticipant(bizuth: Bizuth, ok: boolean): Observable<Bizuth> {
    return this.http.patch<Bizuth>(this.base + 'bizuths/' + bizuth.member.id + '/', {
      participant: ok,
    });
  }

  patchMoyenPaiement(bizuth: Bizuth, payment_method: number): Observable<Bizuth> {
    return this.http.patch<Bizuth>(this.base + 'bizuths/' + bizuth.member.id + '/', {
      payment_method: payment_method,
    });
  }

  filterBungalowForBus(busId: number, bungalows: Bungalow[], bizuth: Bizuth) {
    return bungalows.filter(
      (bungalow: Bungalow) => bungalow.bus === busId && (bungalow.genre === bizuth.member.gender || bungalow.genre === 'I') && bungalow.places_restantes > 0);
  }

  getInfoWei(): Observable<InfosWei> {
    return this.http.get<InfosWei>(environment.adhesion_api_url + '/infos/');
  }

  getListBizBus(): Observable<Bizuth[]> {
    return this.http.get<Bizuth[]>(this.base + 'bus/allbizuths');
  }

  getListBizBungalow(): Observable<Bizuth[]> {
    return this.http.get<Bizuth[]>(this.base + 'bungalow/allbizuths');
  }

  getListBizSpeciaux(): Observable<Bizuth[]> {
    return this.http.get<Bizuth[]>(this.base + 'bizuths/' + 'exportRegimes')
  }

  getListBizParticipants(): Observable<Bizuth[]> {
    return this.http.get<Bizuth[]>(this.base + 'bizuths/' + 'exportBizuths')
  }
}
