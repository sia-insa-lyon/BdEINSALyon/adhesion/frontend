import { Injectable } from '@angular/core';


@Injectable()
export class CsvService {

    downloadFile(data: Object[], headerList: string[], columnName: string[],filename: string ='data') {
        let csvData = this.ConvertToCSV(data, headerList,columnName);
        let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
        let dwldLink = document.createElement("a");
        let url = URL.createObjectURL(blob);
        let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
        if (isSafariBrowser) {  // Gère le cas d'ouverture avec Safari qui va l'enregistrer 
            dwldLink.setAttribute("target", "_blank");
        }
        dwldLink.setAttribute("href", url);
        dwldLink.setAttribute("download", filename + ".csv");
        dwldLink.style.visibility = "hidden";
        document.body.appendChild(dwldLink);
        dwldLink.click();
        document.body.removeChild(dwldLink);
    }

    ConvertToCSV(objArray: Object[], headerList: string[], columnName: string[]) {
         let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
         let str = '';
         let row = 'Numero ;';

         for (let index in columnName) {
             row += columnName[index] + ';';
         }
         row = row.slice(0, -1);
         str += row + '\r\n';
         for (let i = 0; i < array.length; i++) {
             let line = (i+1)+'';
             for (let index in headerList) {
                let head = headerList[index];
                 if(array[i][head] != null){
                    if(array[i][head] === true){
                        line += ';' + '\"'+'OUI'+ '\"';
                    } else if (array[i][head] === false){
                        line += ';' + '\"'+'NON'+ '\"';
                    } else {
                        line += ';' + '\"'+array[i][head]+ '\"';
                    }
                    
                 } else{
                    line += ';';
                 }
             }
             str += line + '\r\n';
         }
         return str;
     }
}