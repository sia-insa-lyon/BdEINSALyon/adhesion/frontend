import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';

/**
 * Service Interface: il sert à passer des données depuis les components "enfants" au app-ccmponent principal
 * Pour l'instant la seule fonctionnalité implémentée est d'afficher le titre de la section utilisée (WEI, ...)
 */
@Injectable()
export class UiService {
  private subject = new Subject<any>();
  private isLoading = new Subject<boolean>();

  setTitle(message: string) {
    this.subject.next(message);
  }

  clearTitle() {
    this.subject.next('Adhesion');
  }

  getTitle(): Observable<any> {
    return this.subject.asObservable();
  }

  getLoadingState(): Observable<boolean> {
    return this.isLoading.asObservable();
  }

  setLoading(loading: boolean) {
    this.isLoading.next(loading);
  }


}
