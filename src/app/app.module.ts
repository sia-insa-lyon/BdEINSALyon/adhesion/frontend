import {APP_INITIALIZER, NgModule} from '@angular/core';
import {KeycloakAngularModule, KeycloakService} from 'keycloak-angular';

import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {MaterialModule} from './material/material.module';
import {UserComponent} from './users/user/user.component';
import {HomeComponent} from './home/home.component';
import {ListComponent} from './members/list/list.component';
import {EditComponent} from './members/edit/edit.component';
import {AuthGuard} from './users/auth-guard.service';
import {CardService, MembershipService, MembersService} from './data/members.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MetadataService} from './data/metadata.service';
import {MemberViewComponent} from './members/view/view.component';
import {CreateComponent} from './members/create/create.component';
import {InteComponent} from './inte/inte.component';
import {UiService} from './data/ui.service';
import {RechercheAdherentComponent} from './members/recherche/recherche.component';
import {MemberShipCreateComponent} from './members/membership/create/create.component';
import {ResetComponent} from './inte/reset/reset.component';
import {ErreurComponent} from './erreur/erreur.component';
import {ChartsModule} from 'ng2-charts';
import {PreenregistrementComponent} from './inte/preenregistrement/preenregistrement.component';
import {CreateCardComponent} from './members/create-card/create-card.component';
import {CompanionComponent} from './companion/companion.component';
import {QRCodeModule} from 'angularx-qrcode';
import {QrViewComponent} from './companion/qr-view/qr-view.component';
import {TableauComponent} from './wei/tableau/tableau.component';
import {WeiComponent} from './wei/wei/wei.component';
import {WeiService} from './data/wei';
import {BizuthEditComponent} from './wei/bizuth-edit/bizuth-edit.component';
import {BusComponent} from './wei/bus/bus.component';
import {CreateMembershipComponent} from './members/membership/create-membership/create-membership.component';
import {BusDetailComponent} from './wei/bus/detail/bus-detail.component';
import {BusEditComponent} from './wei/bus/edit/bus-edit.component';
import {BungalowListComponent} from './wei/bungalow/bungalow-list/bungalow-list.component';
import {BungalowDetailComponent} from './wei/bungalow/bungalow-detail/bungalow-detail.component';
import {BungalowEditComponent} from './wei/bungalow/bungalow-edit/bungalow-edit.component';
import {WeiChequeReceivedBtnComponent} from './wei/bizuth-edit/wei-cheque-received-btn/wei-cheque-received-btn.component';
import {VaChequeReceivedBtnComponent} from './members/view/va-cheque-received-btn/va-cheque-received-btn.component';
import {VaChequeReceivedInfoSpanComponent} from './members/view/va-cheque-received-info-span/va-cheque-received-info-span.component';
import {WeiChequeReceivedInfoSpanComponent} from './wei/bizuth-edit/wei-cheque-received-info-span/wei-cheque-received-info-span.component';
// tslint:disable-next-line:max-line-length
import {WeiAuthorisationParentaleBtnComponent} from './wei/bizuth-edit/wei-authorisation-parentale-btn/wei-authorisation-parentale-btn.component';
// tslint:disable-next-line:max-line-length
import {WeiAuthorisationParentaleInfoSpanComponent} from './wei/bizuth-edit/wei-authorisation-parentale-info-span/wei-authorisation-parentale-info-span.component';
import {WeiCautionInfoSpanComponent} from './wei/bizuth-edit/wei-caution-info-span/wei-caution-info-span.component';
import {WeiCautionBtnComponent} from './wei/bizuth-edit/wei-caution-btn/wei-caution-btn.component';
import {DisableCardBtnComponent} from './members/disable-card-btn/disable-card-btn.component';
import {WeiRemarquesRegimeFieldsComponent} from './wei/bizuth-edit/wei-remarques-regime-fields/wei-remarques-regime-fields.component';
import {WeiBusBungalowFieldsComponent} from './wei/bizuth-edit/wei-bus-bungalow-fields/wei-bus-bungalow-fields.component';
import {WeiDeleteBizuthComponent} from './wei/bizuth-edit/wei-delete-bizuth/wei-delete-bizuth.component';
import {WeiMoyenPaiementComponent} from './wei/bizuth-edit/wei-moyen-paiement/wei-moyen-paiement.component';
import {MembershipFeuilleCaisseComponent} from './members/membership/membership-feuille-caisse/membership-feuille-caisse.component';
import {environment} from '../environments/environment';
import {MercanetSearchComponent} from './compta/mercanet/mercanet-search/mercanet-search.component';
import {MercanetService} from './data/mercanet.service';
import {ControlPanelService, LoggerService, WatcherTowerService} from './data/management.service';
import {WatchTowerComponent} from './mgmt/watchtower/watch-tower.component';
import {CdpDetailComponent} from './mgmt/cdp-detail/cdp-detail.component';
import {ChatMessagePopupComponent} from './mgmt/chat-message-popup/chat-message-popup.component';
import {ControlPanelComponent} from './mgmt/control-panel/control-panel.component';
import {MaintenanceComponent} from './mgmt/maintenance/maintenance.component';
import {GenerateCodeComponent} from './generate-code/generate-code.component';
import {MatTableExporterModule} from 'mat-table-exporter';
import {CsvService} from './data/csv.service';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ListCardsComponent } from './members/list-cards/list-cards.component';
import { ListMembershipsComponent } from './members/membership/list-memberships/list-memberships.component';

export function initializeKeycloak(keycloak: KeycloakService) {
  return () =>
    keycloak.init({
      config: {
        url: environment.keycloak_url,
        realm: environment.keycloak_realm,
        clientId: environment.keycloak_client_id,
      },
      initOptions: {
        onLoad: 'check-sso',
        checkLoginIframe: true,
        flow: 'standard',
        silentCheckSsoRedirectUri: window.location.origin + '/assets/silent-check-sso.html', // connexion auto
      },
    });
}

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    HomeComponent,
    ListComponent,
    RechercheAdherentComponent,
    EditComponent,
    MemberViewComponent,
    CreateComponent,
    InteComponent,
    MemberShipCreateComponent,
    ResetComponent,
    ErreurComponent,
    PreenregistrementComponent,
    CreateCardComponent,
    CompanionComponent,
    QrViewComponent,
    TableauComponent,
    WeiComponent,
    BizuthEditComponent,
    BusComponent,
    CreateMembershipComponent,
    BusDetailComponent,
    BusEditComponent,
    BungalowListComponent,
    BungalowDetailComponent,
    BungalowEditComponent,
    WeiChequeReceivedBtnComponent,
    VaChequeReceivedBtnComponent,
    VaChequeReceivedInfoSpanComponent,
    WeiChequeReceivedInfoSpanComponent,
    WeiAuthorisationParentaleBtnComponent,
    WeiAuthorisationParentaleInfoSpanComponent,
    WeiCautionInfoSpanComponent,
    WeiCautionBtnComponent,
    DisableCardBtnComponent,
    WeiRemarquesRegimeFieldsComponent,
    WeiBusBungalowFieldsComponent,
    WeiDeleteBizuthComponent,
    WeiMoyenPaiementComponent,
    MembershipFeuilleCaisseComponent,
    MercanetSearchComponent,
    GenerateCodeComponent,
    WatchTowerComponent,
    CdpDetailComponent,
    ChatMessagePopupComponent,
    ControlPanelComponent,
    MaintenanceComponent,
    ListCardsComponent,
    ListMembershipsComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatTooltipModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    ChartsModule,
    MaterialModule,
    QRCodeModule,
    KeycloakAngularModule,
    MatTableExporterModule
  ],
  providers: [
    AuthGuard,
    MembersService,
    MembershipService,
    MetadataService,
    CardService,
    UiService,
    WeiService,
    MercanetService,
    CsvService,
    LoggerService,
    WatcherTowerService,
    ControlPanelService,
//    { provide: LOCALE_ID, useValue: 'fr' }
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
    },
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    QrViewComponent,
  ]
})
export class AppModule {
}
