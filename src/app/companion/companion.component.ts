import {Component, OnInit} from '@angular/core';
import {CompanionService} from '../data/companion.service';
import {CardService} from '../data/members.service';

@Component({
  selector: 'app-companion',
  templateUrl: './companion.component.html',
  styleUrls: ['./companion.component.scss']
})
export class CompanionComponent implements OnInit {
  historique: String[] = [];
  error = '';
  completed = false;
  memberId: number = null;
  access_token = sessionStorage.getItem('access_token');
  phoneConnected = false;

  constructor(private sync: CompanionService, private capi: CardService) {
  }

  ngOnInit() {
    this.sync.observableSync.subscribe(data => {
        this.historique.push(JSON.stringify(data));
        console.log(data);
        if (/c([0-9]{12})/.test(data['code']) && data['type'] === 'code_va') {
          this.historique.push('Code VA, essai de récup');
          this.capi.getMember(data['code']).subscribe(
            m => {
              this.historique.push(m.first_name + ' ' + m.last_name);
              this.memberId = m.id;
            },
            er => {
              this.historique.push('erreur ' + er.toString());
            }
          )
          ;
        }
        if (data['type'] === 'status_message' && data['status'] === 'connected') {
          this.phoneConnected = true;
        }
      },
      error => {
        this.error = error;
      },
      () => {
        this.completed = true;
      });
    this.sync.testsend();
  }
}
