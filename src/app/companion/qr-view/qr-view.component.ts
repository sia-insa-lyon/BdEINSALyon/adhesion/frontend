import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-qr-view',
  templateUrl: './qr-view-component.html',
})
export class QrViewComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: string) {
  }

  ngOnInit() {
  }

}
