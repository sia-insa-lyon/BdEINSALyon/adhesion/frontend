// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  adhesion_api_url: 'http://localhost:8000/v1',
  adhesion_url: 'http://localhost:8000',
  mobile_gateway_url: 'ws://localhost:8002',
  keycloak_client_id: 'adhesion-admin-frontend',
  keycloak_realm: 'asso-insa-lyon',
  keycloak_url: 'http://localhost:8080/auth',
  mgmt_websocket: 'ws://localhost:8090',
  mgmt_endpoint: 'http://localhost:8090',
  mgmt_role: 'mgmt'
};
