export const environment = {
  production: true,
  adhesion_api_url: '__ADHESION_API_URL__',
  adhesion_url: '__ADHESION_URL__',
  wei_api_url: '__WEI_API_URL__',
  mobile_gateway_url: '__MOBILE_GATEWAY_URL__',
  keycloak_client_id: '__KEYCLOAK_CLIENT_ID__',
  keycloak_realm: '__KEYCLOAK_REALM__',
  keycloak_url: '__KEYCLOAK_URL__',
  mgmt_websocket: '__MGMT_WS_URL__',
  mgmt_endpoint: '__MGMT_URL__',
  mgmt_role: '__MGMT_ROLE__',
};
