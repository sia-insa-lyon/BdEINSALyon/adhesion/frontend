# Adhesion Frontend

This application is a Web client for student union managers to access to 
data stored onto an Adhesion API server.

It's developed with Angular 11. Project is managed through Angular CLI
and deployed with GitLab CI through Docker containers.
# How to use it:

If you want to connect your frontend to a distant or a local API you must change those variables:
```
ADHESION_API_URL
ADHESION_URL
WEI_API_URL
KEYCLOAK_CLIENT_ID
KEYCLOAK_REALM
KEYCLOAK_URL
```
---
## Keycloak setup

If you wish to connect to the Membership API you will need a valid user account  
*[(See Adhesion API verification)](https://gitlab.com/sia-insa-lyon/BdEINSALyon/adhesion/api/-/blob/master/README.md#for-jwt-authentication)*


Two solutions:
1. 
   - You can go through the public interface to create your account.
   - Give the right permissions of `adhesion-api` (minimum: `staff`) to your new account with the Keycloak admin interface
   - Connect to the admin interface  
  

2.
   - Create directly an account on Keycloak ("User")
   - Add a password, email, and add the permissions of `adhesion-api` needed (minimum: `staff`)
   - Add `adhesionUserId` and `memberId` attribute to create the link with a member on adhesion's database (you can take the id of a dummy account created by `seeddb`, i.e: `1`)

--- 
## Deployment documentation
Being conform with the [12factor](https://12factor.net) is not quite
easy with an Angular application because it's frontend code pre-compiled
on the server-side.

The main concerns is to use environment variables with the application.
To achieve that, we have used a trick.

We build a docker image based on Nginx containing the compiled code of
our application but the start script is design to use `sed` command
to replace some text into the code (which is string formated) by values
from environment. This can be easily modified to prefer Docker secrets.

--- 
## Some useful commands

- Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
- Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.
- Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

---
## License

This software is provided under GNU AGPLv3 license

## Contributors
```
© 2018 Philippe VIENNE <philippegeek@gmail.com>
© 2019 Hugo REYMOND
© 2020 Jean RIBES <jean@ribes.ovh>
© 2021 Baptiste LALANNE <baptiste.lalanne.pro@protonmail.com>
```

